/*******************************************************************
 *
 *
 * Name: Lirik_CS_Shipping_Request.js
 * Script Type: Client Script
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script will work on validation on Shipping Parent Form
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */
function callInventoryAdjustment() {
  var getshippingChildRecs = searchMTShippingDetails(nlapiGetRecordId());
  var flag = false;
  if (getshippingChildRecs) {
    for (var childIndex = 0; childIndex < getshippingChildRecs.length; childIndex++) {
      var itemVal = getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_item');
      if (itemVal) {
        var fieldArr = ['islotitem', 'isserialitem'];
        var getItemType = nlapiLookupField('item', itemVal, fieldArr);
        if ((getItemType.islotitem == 'T' || getItemType.isserialitem == 'T') && !(getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_inventorydet'))) {
          flag = false;
          alert('Please enter inventory details for Serial/Lot item.');
          return false;
        } else if (!(getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_invent_accno'))) {
          flag = false;
          alert('Please select an account number.');
          return false;
        } else if (!(getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_quantity'))) {
          flag = false;
          alert('Please select a quantity.');
          return false;
        } else if (!(getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_stock'))) {
          flag = false;
          alert('Please select Into or Out to stock.');
          return false;
        } else {
          flag = true;
        }
      }
      else if (!(getshippingChildRecs[childIndex].getValue('custrecord_shipping_details_item'))) {
        flag = false;
        alert('Please select an item.');
        return false;
      }
    }
  } else {
    alert('No record found for inventory adjustment.');
    return false;
  }
  if (flag == true) {
    var invAdjUrl = nlapiResolveURL('SUITELET', 'customscript_lirik_create_inv_adj_sui', 'customdeploy_lirik_create_inv_adj_sui');
    invAdjUrl = invAdjUrl + '&shipreqid=' + nlapiGetRecordId();
    window.open(invAdjUrl, '_self');
  }
}
function callSOInactive(){
  var getSoandInactive = searchSOChildShippingRecords(nlapiGetRecordId());
  var flag = false;
  if (getSoandInactive) {
    for (var childIndex = 0; childIndex < getSoandInactive.length; childIndex++) {
      var itemVal = getSoandInactive[childIndex].getValue('custrecord_shipping_details_item');
      if (itemVal) {
        var fieldArr = ['islotitem', 'isserialitem'];
        var getItemType = nlapiLookupField('item', itemVal, fieldArr);
        /*if ((getItemType.islotitem == 'T' || getItemType.isserialitem == 'T') && !(getSoandInactive[childIndex].getValue('custrecord_shipping_details_inventorydet'))) {
          flag = false;
          alert('Please enter inventory details for Serial/Lot item.');
          return false;
        } */
          /*if (!(getSoandInactive[childIndex].getValue('custrecord_shipping_details_invent_accno'))) {
          flag = false;
          alert('Please select an account number.');
          return false;
        }*/
          if (!(getSoandInactive[childIndex].getValue('custrecord_shipping_details_quantity'))) {
          flag = false;
          alert('Please select a quantity.');
          return false;
        } else if (!(getSoandInactive[childIndex].getValue('custrecord_shipping_details_stock'))) {
          flag = false;
          alert('Please select Into or Out to stock.');
          return false;
        } else {
          flag = true;
        }
      }
      else if (!(getSoandInactive[childIndex].getValue('custrecord_shipping_details_item'))) {
      flag = false;
      alert('Please select an item.');
      return false;
    }
    }
  }
  else {
    alert('No record found.');
    return false;
  }
  if (flag == true) {
    var soUrl = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_create_salesorder', 'customdeploy_lirik_ss_create_sales_order');
    soUrl = soUrl + '&shipreqid=' + nlapiGetRecordId();
    window.open(soUrl, '_self');
  }
}
//Function call when 'Approve' button is clicked
function approveRequest() {
  var context = nlapiGetContext();
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_approve=T';
  var setIsApprove = nlapiRequestURL(suiteletURL, null, null, 'GET');
  window.location.reload();
}

//Function call when 'Reject' button is clicked
function rejectRequest() {
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_reject=T';
  var setIsApprove = nlapiRequestURL(suiteletURL, null, null, 'GET');
  window.location.reload();
}

function sendEmailToRequester() {
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_sendtorequestor=T';
  var callToSendEmail = nlapiRequestURL(suiteletURL, null, null, 'GET');
}

function sendNoteToWarehouseMgr() {
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_sendtowhsemgr=T';
  var callToSendEmail = nlapiRequestURL(suiteletURL, null, null, 'GET');
}

function sendToCustomer() {
  //alert('In Send To Customer');
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_sendtocustomer=T';
  var callToSendEmail = nlapiRequestURL(suiteletURL, null, null, 'GET');
}

function sendNoteToLogManager() {
  var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid=' + nlapiGetRecordId() + '&custparam_sendtologmgr=T';
  var callToSendEmail = nlapiRequestURL(suiteletURL, null, null, 'GET');
}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

function searchMTShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];
    shippingFilters.push(new nlobjSearchFilter('custrecord_now_future_never', null, 'is', '1'));
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', '1'), new nlobjSearchFilter('custrecord_shipping_details_inventoryadj', null, 'is', 'T'), new nlobjSearchFilter('custrecord_lirik_inv_adj_recs', null, 'anyof', '@NONE@'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;
}

function searchSOChildShippingRecords(shipReqId){
  var shipReqDetailFil = [],
    shipReqDetailCol = [];
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_now_future_never', null, 'anyof', ['2', '3']));
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shipReqId));
  shipReqDetailFil.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', '1'));
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_lirik_inv_adj_recs', null, 'anyof', '@NONE@'));
  //if (loggedInRole == 'customrole1027') // Role is "LRI Warehouse Manager"
  //shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_inventoryadj', null, 'is', 'T'));

  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_item'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_stock'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_invent_accno'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_quantity'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_now_future_never'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_description'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_department', 'CUSTRECORD_SHIPPING_REQUEST_ID'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_customer', 'CUSTRECORD_SHIPPING_REQUEST_ID'));

  var shipReqDetailRslts = nlapiSearchRecord('customrecord_shipping_request_details', null, shipReqDetailFil, shipReqDetailCol);
  return shipReqDetailRslts;
}
