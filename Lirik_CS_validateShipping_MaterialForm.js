/*******************************************************************
 *
 *
 * Name: Lirik_CS_validateShipping_MaterialForm.js
 * Script Type: Client
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to validate shipping request form.
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */


//Page Init Call call when requester wants to edit the xisting request.
function pageinit() {
  var lineno = nlapiGetCurrentLineItemIndex('custpage_details');
  nlapiSetCurrentLineItemValue('custpage_details', 'custpage_lineseq', parseInt(lineno), false, false);

  if (window.getParameter('custparam_requestid')) {
      if (nlapiGetFieldValue('custpage_trantype') == '1') {
        if (nlapiGetLineItemCount('custpage_details') > 0) {
          for (var lineIndex = 1; lineIndex <= nlapiGetLineItemCount('custpage_details'); lineIndex++) {
            nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', true);
            nlapiSetLineItemValue('custpage_details', 'custpage_adjuststock', lineIndex, '1');
          }
        } else {
          nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', true);
          nlapiSetCurrentLineItemValue('custpage_details', 'custpage_adjuststock', '1');
        }
      } else if (nlapiGetFieldValue('custpage_trantype') != '1') {
        if (nlapiGetLineItemCount('custpage_details') > 0) {
          for (var lineIndex = 1; lineIndex <= nlapiGetLineItemCount('custpage_details'); lineIndex++) {
            nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', false);
          }
        } else {
          nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', false);
        }
      }

			var customerVal = nlapiGetFieldValue('custpage_customer');
			var empVal = nlapiGetFieldValue('custpage_employee')
			if (customerVal) {
				var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_customer=' + customerVal;
				var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
				var customerSearch = requestObj.getBody();
				var shippingAdd = JSON.parse(customerSearch);
				nlapiSetFieldValue('custpage_shiptoadd', shippingAdd.custAdd, false, false);

				//Code added to source project field
				var removeAccOptionArr = [];
				var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_customer=' + customerVal + '&custparam_projectsearch=T';
				var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
				var projectSearch = requestObj.getBody();
				console.log('projectSearch-->' + projectSearch);
				var projectSearchDetails = JSON.parse(projectSearch);


				if (nlapiGetFieldValue('custpage_project_length')) {
					var currentProjects = nlapiGetFieldValue('custpage_project_length').split(',');
					for (var projectIndex = 0; currentProjects && projectIndex < currentProjects.length; projectIndex++) {
						//nlapiRemoveSelectOption('custpage_account',accountLength[projectIndex]);
						nlapiRemoveLineItemOption('custpage_details', 'custpage_project', currentProjects[projectIndex]);
					}
					nlapiSetFieldValue('custpage_project_length', '');
				}

				if (projectSearchDetails && projectSearchDetails.length > 0) {
					for (var i = 0; i < projectSearchDetails.length; i++) {
						var intID = projectSearchDetails[i].internalId;
						var projectName = projectSearchDetails[i].projectName;
						//nlapiInsertSelectOption('custpage_account',intID, listVal1[i].getValue('name'),false);
						nlapiInsertLineItemOption('custpage_details', 'custpage_project', intID, projectName, false);
						removeAccOptionArr.push(intID);
					}
					nlapiSetFieldValue('custpage_project_length', removeAccOptionArr.join());
				}
				//Code ended for project field
			}
			else if(empVal){
				//Code added to source project field
        var removeAccOptionArr = [];
        var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_emp_projectsearch=T';
        var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
        var projectSearch = requestObj.getBody();
        console.log('projectSearch-->' + projectSearch);
        var projectSearchDetails = JSON.parse(projectSearch);


        if (nlapiGetFieldValue('custpage_project_length')) {
          var currentProjects = nlapiGetFieldValue('custpage_project_length').split(',');
          for (var projectIndex = 0; currentProjects && projectIndex < currentProjects.length; projectIndex++) {
            //nlapiRemoveSelectOption('custpage_account',accountLength[projectIndex]);
            nlapiRemoveLineItemOption('custpage_details', 'custpage_project', currentProjects[projectIndex]);
          }
          nlapiSetFieldValue('custpage_project_length', '');
        }

        if (projectSearchDetails && projectSearchDetails.length > 0) {
          for (var i = 0; i < projectSearchDetails.length; i++) {
            var intID = projectSearchDetails[i].internalId;
            var projectName = projectSearchDetails[i].projectName;
            //nlapiInsertSelectOption('custpage_account',intID, listVal1[i].getValue('name'),false);
            nlapiInsertLineItemOption('custpage_details', 'custpage_project', intID, projectName, false);
            removeAccOptionArr.push(intID);
          }
          nlapiSetFieldValue('custpage_project_length', removeAccOptionArr.join());
        }
        //Code ended for project field
			}

    }
  }

  function clientValidateLine(type) {
    try {
      var deldate = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_deliveryddate');
      var shipdate = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_shipdate');
      var carnetDesired = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_carnet');
      var carnetDetails = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_carnetdetails');
      var projectValue = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_project');
      var customerValue = nlapiGetFieldValue('custpage_customer');
      var employeeValue = nlapiGetFieldValue('custpage_employee');

      var item = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_item');
      if (!deldate && !shipdate) {
        alert('Please enter Requested Delivery Date or Requested Shipping Date (or both).');
        return false;
      } else if (carnetDesired == 1 && !carnetDetails) {
        alert('Please enter Carnet Details.');
        return false;
      } else if (adjustStock == 1 && !item) {
        alert('Please select an item.');
        return false;
      }
      // else if (!(customerValue) && !(employeeValue) && !(projectValue)) {
      //   alert('Please select a customer/employee first to get projects.');
      //   return false;
      // }
      else {
        return true;
      }
    } catch (e) {
      console.log(e);
    }
    return true;
  }

  /**
   * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
   * @appliedtorecord recordType
   *
   * @param {String} type Sublist internal id
   * @param {String} name Field internal id
   * @param {Number} linenum Optional line item number, starts from 1
   * @returns {Void}
   */
  function clientFieldChanged(type, name, linenum) {
    try {
      if (name == 'custpage_trantype') {
        if (nlapiGetFieldValue('custpage_trantype') == '1') {
          if (nlapiGetLineItemCount('custpage_details') > 0) {
            for (var lineIndex = 1; lineIndex <= nlapiGetLineItemCount('custpage_details'); lineIndex++) {
              nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', true);
              nlapiSetLineItemValue('custpage_details', 'custpage_adjuststock', lineIndex, '1');
            }
          } else {
            nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', true);
            nlapiSetCurrentLineItemValue('custpage_details', 'custpage_adjuststock', '1');
          }
        } else if (nlapiGetFieldValue('custpage_trantype') != '1') {
          if (nlapiGetLineItemCount('custpage_details') > 0) {
            for (var lineIndex = 1; lineIndex <= nlapiGetLineItemCount('custpage_details'); lineIndex++) {
              nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', false);
            }
          } else {
            nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', false);
          }
        }
      }
      var lineno = nlapiGetCurrentLineItemIndex('custpage_details');
      //alert(lineno);
      if (lineno && lineno != -1 && !(window.getParameter('custparam_requestid')))
        nlapiSetCurrentLineItemValue('custpage_details', 'custpage_lineseq', parseInt(lineno), false, false);
      if (name == 'custpage_adjuststock') {
        var adjustStock = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_adjuststock');
        if (adjustStock == 1) {
          nlapiSetCurrentLineItemValue('custpage_details', 'custpage_shipmentstatus', '2', false, false);
        } else {
          nlapiSetCurrentLineItemValue('custpage_details', 'custpage_shipmentstatus', '1', false, false);
        }
      }
      if (name == 'custpage_des') {
        var itemdes = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_des');
        if (itemdes) {
          var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_itemDes=' + itemdes;
          var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
          var itemSearch = requestObj.getBody();
          var itemDetails = JSON.parse(itemSearch);
          //alert(itemSearch);
          if (itemSearch) {
            nlapiSetCurrentLineItemValue('custpage_details', 'custpage_item', itemDetails.itemid, false, false);
            nlapiSetCurrentLineItemValue('custpage_details', 'custpage_itemcost', itemDetails.locCost, false, false);
          }
        }
      } else if (name == 'custpage_item') {
        var itemVal = nlapiGetCurrentLineItemValue('custpage_details', 'custpage_item');
        if (itemVal) {
          var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_itemval=' + itemVal;
          var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
          var itemSearch = requestObj.getBody();
          var itemDetails = JSON.parse(itemSearch);
          //alert(itemSearch);
          if (itemSearch) {
            //nlapiSetCurrentLineItemValue('custpage_details', 'custpage_item', itemDetails.itemid, false, false);
            nlapiSetCurrentLineItemValue('custpage_details', 'custpage_itemcost', itemDetails.locCost, false, false);
          }
        }
      }
			if(name == 'custpage_nowfutnever'){
				var nowFutNever = nlapiGetCurrentLineItemValue('custpage_details','custpage_nowfutnever');
				if(nowFutNever == '1'){
					nlapiSetCurrentLineItemValue('custpage_details','custpage_adjuststock','1',false,false);
				}
				else{
					nlapiSetCurrentLineItemValue('custpage_details','custpage_adjuststock','2',false,false);
				}
			}
      if (name == 'custpage_customer') {
        var custId = nlapiGetFieldValue('custpage_customer');
        //alert(custId);
        nlapiSetFieldValue('custpage_employee', '', false, false);

        var customerVal = nlapiGetFieldValue('custpage_customer');
        if (customerVal) {
          var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_customer=' + customerVal;
          var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
          var customerSearch = requestObj.getBody();
          var shippingAdd = JSON.parse(customerSearch);
          nlapiSetFieldValue('custpage_shiptoadd', shippingAdd.custAdd, false, false);

          //Code added to source project field
          var removeAccOptionArr = [];
          var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_customer=' + customerVal + '&custparam_projectsearch=T';
          var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
          var projectSearch = requestObj.getBody();
          console.log('projectSearch-->' + projectSearch);
          var projectSearchDetails = JSON.parse(projectSearch);


          if (nlapiGetFieldValue('custpage_project_length')) {
            var currentProjects = nlapiGetFieldValue('custpage_project_length').split(',');
            for (var projectIndex = 0; currentProjects && projectIndex < currentProjects.length; projectIndex++) {
              //nlapiRemoveSelectOption('custpage_account',accountLength[projectIndex]);
              nlapiRemoveLineItemOption('custpage_details', 'custpage_project', currentProjects[projectIndex]);
            }
            nlapiSetFieldValue('custpage_project_length', '');
          }

          if (projectSearchDetails && projectSearchDetails.length > 0) {
            for (var i = 0; i < projectSearchDetails.length; i++) {
              var intID = projectSearchDetails[i].internalId;
              var projectName = projectSearchDetails[i].projectName;
              //nlapiInsertSelectOption('custpage_account',intID, listVal1[i].getValue('name'),false);
              nlapiInsertLineItemOption('custpage_details', 'custpage_project', intID, projectName, false);
              removeAccOptionArr.push(intID);
            }
            nlapiSetFieldValue('custpage_project_length', removeAccOptionArr.join());
          }
          //Code ended for project field
        }
      }
      if (name == 'custpage_employee') {
        nlapiSetFieldValue('custpage_customer', null, false, false);
        nlapiSetFieldValue('custpage_shiptoadd', null, false, false);

        //Code added to source project field
        var removeAccOptionArr = [];
        var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_emp_projectsearch=T';
        var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
        var projectSearch = requestObj.getBody();
        console.log('projectSearch-->' + projectSearch);
        var projectSearchDetails = JSON.parse(projectSearch);


        if (nlapiGetFieldValue('custpage_project_length')) {
          var currentProjects = nlapiGetFieldValue('custpage_project_length').split(',');
          for (var projectIndex = 0; currentProjects && projectIndex < currentProjects.length; projectIndex++) {
            //nlapiRemoveSelectOption('custpage_account',accountLength[projectIndex]);
            nlapiRemoveLineItemOption('custpage_details', 'custpage_project', currentProjects[projectIndex]);
          }
          nlapiSetFieldValue('custpage_project_length', '');
        }

        if (projectSearchDetails && projectSearchDetails.length > 0) {
          for (var i = 0; i < projectSearchDetails.length; i++) {
            var intID = projectSearchDetails[i].internalId;
            var projectName = projectSearchDetails[i].projectName;
            //nlapiInsertSelectOption('custpage_account',intID, listVal1[i].getValue('name'),false);
            nlapiInsertLineItemOption('custpage_details', 'custpage_project', intID, projectName, false);
            removeAccOptionArr.push(intID);
          }
          nlapiSetFieldValue('custpage_project_length', removeAccOptionArr.join());
        }
        //Code ended for project field
      }
    } catch (e) {
      console.log(e);
    }
  }


  function recalculateTotal() {
    try {
      var totalamt = 0.00;
      //alert('Count-->' + nlapiGetLineItemCount('custpage_details'));
      for (var i = 1; i <= nlapiGetLineItemCount('custpage_details'); i++) {
        var adjustStock = nlapiGetLineItemValue('custpage_details', 'custpage_adjuststock', i);
        //alert('adjustStock>>>>>' + adjustStock + 'Current Line Item Count>>>>>' + nlapiGetCurrentLineItemIndex('custpage_details'));
        if (adjustStock == 1) {
          var amount = nlapiGetLineItemValue('custpage_details', 'custpage_itemcost', i);
          if (amount)
            totalamt = parseFloat(totalamt) + parseFloat(amount); //parseFloat(nlapiGetFieldValue('custpage_total'));
        }
      }
      nlapiSetFieldValue('custpage_total', totalamt);
    } catch (e) {
      console.log(e);
    }
  }

  function shippingLineInit(type) {
    if (nlapiGetFieldValue('custpage_trantype') == '1') {
      //nlapiDisableLineItemField('custpage_details', 'custpage_adjuststock', true);
      nlapiSetCurrentLineItemValue('custpage_details', 'custpage_adjuststock', '1');
    }
		var lineno = nlapiGetCurrentLineItemIndex('custpage_details');
		//alert(lineno);
		if (lineno && lineno != -1)
			nlapiSetCurrentLineItemValue('custpage_details', 'custpage_lineseq', parseInt(lineno), false, false);
  }

  function shippingLineRemove(type){
    //alert(window.getParameter('custparam_requestid'));
    if(type == 'custpage_details' && window.getParameter('custparam_requestid')){
      var deleteChildRecord = confirm('Are you sure to remove this line.Click OK to remove this line or click Cancel to abort this operation.');
      //alert(deleteChildRecord);
      if(deleteChildRecord){
        var lineSequenceNo = nlapiGetCurrentLineItemValue('custpage_details','custpage_lineseq');
        var parentShipRecord = window.getParameter('custparam_requestid');
        //alert(parentShipRecord)
        var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_parentid='+parentShipRecord+'&custparam_linesequenceno='+lineSequenceNo;
        var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
      }
    }
    return true;
  }
