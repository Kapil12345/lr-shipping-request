function editRequestPageInit(){
  try{
    var isApproved = nlapiGetFieldValue('custpage_isapproved');
    if(isApproved != '1'){
      var detailsLineCount = nlapiGetLineItemCount('custpage_details');
      for(var i=1; i<=detailsLineCount; i++){
        nlapiSetLineItemDisabled('custpage_details', 'custpage_ready_forinvadj', true,i);
      }
    }
  }
  catch(e){
    console.log(e);
  }
}


function editRequestSaveRecord(){
	try {
  //alert(window.getParameter('custparam_mtrole'));
  if(window.getParameter('custparam_warehouserole') && window.getParameter('custparam_warehouserole') == nlapiGetRole()){//
    for(var lineindex=1; lineindex>0 && lineindex<=nlapiGetLineItemCount('custpage_details'); lineindex++){
      var itemVal = nlapiGetLineItemValue('custpage_details','custpage_item',lineindex);
      if(itemVal){
        var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_backend_suitelet', 'customdeploy_lirik_ss_backend_suitelet') + '&custparam_itemVal=' + itemVal;
        var requestObj = nlapiRequestURL(suiteletURL, null, null, 'GET');
        var itemSearch = requestObj.getBody();
        alert(itemSearch);
        if(itemSearch == 'T' && !(nlapiGetLineItemValue('custpage_details','custrecord_shipping_details_inventorydet',lineindex))){
          alert('Please enter inventory details for Serial/Lot item');
            return false;
        }
      }
    }
  }
return true;
} catch (e) {
  console.log(e);
}
}

function editRequestFieldChanged(type,name,lineNum){
  if(name=='custpage_isapproved'){
    var isApproved = nlapiGetFieldValue('custpage_isapproved');
    var detailsLineCount = nlapiGetLineItemCount('custpage_details');
    if(isApproved != '1'){
      for(var i=1; i<=detailsLineCount; i++){
        nlapiSetLineItemValue('custpage_details', 'custpage_ready_forinvadj', i,'F');
        nlapiSetLineItemDisabled('custpage_details', 'custpage_ready_forinvadj', true,i);
      }
    }
    else{
      for(var i=1; i<=detailsLineCount; i++){
        nlapiSetLineItemDisabled('custpage_details', 'custpage_ready_forinvadj', false,i);
      }
    }
  }
  else if(name == 'custpage_customer'){}
}

/*function editRequestRecalculateTotal(){
	try {
  var totalamt = 0.00;
  //alert('Count-->' + nlapiGetLineItemCount('custpage_details'));
  for (var i = 1; i <= nlapiGetLineItemCount('custpage_details'); i++) {
    var adjustStock = nlapiGetLineItemValue('custpage_details', 'custpage_adjuststock', i);
    //alert('adjustStock>>>>>' + adjustStock + 'Current Line Item Count>>>>>' + nlapiGetCurrentLineItemIndex('custpage_details'));
    if (adjustStock == 1) {
      var amount = nlapiGetLineItemValue('custpage_details', 'custpage_itemcost', i);
      totalamt = parseFloat(totalamt) + parseFloat(amount); //parseFloat(nlapiGetFieldValue('custpage_total'));
    }
  }
  nlapiSetFieldValue('custpage_total', totalamt);
} catch (e) {
  console.log(e);
}
}
*/

/*function getParameterFromURL(param){
var query = window.location.search.substring(1);
var vars = query.split("&");
for (var i=0;i<vars.length;i++) {
var pair = vars[i].split("=");
if(pair[0] == param){
return decodeURIComponent(pair[1]);}
}
return(false);
}*/
