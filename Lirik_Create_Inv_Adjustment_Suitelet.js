/**
 * File : Lirik_Create_Inv_Adjustment_Suitelet.js (version 1.0)
 * Suitelet script to create Inventory adjustment on button click from shipping request record
 * Client: Liquid Robotics
 * Author: DBablu
 * Date: 14th Dec 2017
 * Company: Lirik Inc.
 *
 */

var loggedInRole = nlapiGetContext().getRoleId();

// Location is "LRI - Sunnyvale", Subsidiary is "Liquid Robotics"
var invAdjLoc = nlapiGetContext().getSetting('SCRIPT', 'custscript_lirik_inv_adj_loc'),
invAdjSub = nlapiGetContext().getSetting('SCRIPT', 'custscript_lirik_inv_adj_sub');

function prepareForInvAdj(request, response) {
	try {
		var shipReqId = request.getParameter('shipreqid');

		// Searching related shipping request detail record
		var shipReqDetailFil = [],
		shipReqDetailCol = [];

		shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shipReqId));
		shipReqDetailFil.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', 'T'));

		//if (loggedInRole == 'customrole1027') // Role is "LRI Warehouse Manager"
		shipReqDetailFil.push(new nlobjSearchFilter('custrecord_now_future_never', null, 'is', '1'));
		shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_inventoryadj', null, 'is', 'T'));
    shipReqDetailFil.push(new nlobjSearchFilter('custrecord_lirik_inv_adj_recs', null, 'anyof', '@NONE@'));

		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_item'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_stock'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_invent_accno').setSort());
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_quantity'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_now_future_never'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_description'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_department', 'CUSTRECORD_SHIPPING_REQUEST_ID'));
		shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_customer', 'CUSTRECORD_SHIPPING_REQUEST_ID'));

		var shipReqDetailRslts = nlapiSearchRecord('customrecord_shipping_request_details', null, shipReqDetailFil, shipReqDetailCol);
		var currentLineAccNo = '',
		prevLineAccNo = '',
		adjRec,
		invAdjRecId,
		arrLineRecordId = [];
		//Code Start for last child
		for (var shipReqCount = 0; shipReqDetailRslts && shipReqCount <= shipReqDetailRslts.length; shipReqCount++) {

			if (shipReqCount === shipReqDetailRslts.length && shipReqDetailRslts[shipReqCount - 1].getText('custrecord_now_future_never') == 'Now') {
				invAdjRecId = nlapiSubmitRecord(adjRec);
				arrLineRecordId.push(shipReqDetailRslts[shipReqCount - 1].getId());
				nlapiLogExecution('DEBUG', 'prepareForInvAdj', 'arrLineRecordId ::' + arrLineRecordId);
				for (var index = 0; index < arrLineRecordId.length; index++) {
					if (invAdjRecId) {
						nlapiSubmitField(shipReqDetailRslts[shipReqCount - 1].getRecordType(), arrLineRecordId[index],
							['custrecord_shipping_details_shipstatus', 'custrecord_lirik_inv_adj_recs'], ['6', invAdjRecId]);
					}
				}

				break;
			}
			//Code End for last child

			var createStatus = shipReqDetailRslts[shipReqCount].getText('custrecord_now_future_never'),
			itemName = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_item'),
			itemQty = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_quantity'),
			itemDesc = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_description'),
			invSerialNo = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_inventorydet'),
			invAcct = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_invent_accno'),
			invAdjType = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_stock'),
			invAdjDept = shipReqDetailRslts[shipReqCount].getValue('custrecord_department', 'CUSTRECORD_SHIPPING_REQUEST_ID'),
			soCust = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_customer', 'CUSTRECORD_SHIPPING_REQUEST_ID');
			currentLineAccNo = invAcct;
			/*try{
			prevLineAccNo = shipReqDetailRslts[shipReqCount-1].getValue('custrecord_shipping_details_invent_accno','CUSTRECORD_SHIPPING_REQUEST_ID');
			}
			catch(e){
			nlapiLogExecution('ERROR','Previous LineAccount No not exist', 'Error avoidable due to  firstline:'+e);
			}*/

			if (createStatus == 'Now') { // Create inventory adjustement if Now/Future/Never field value is "Now".
				//Initiate Inventory Adjustment record creation

				if (prevLineAccNo != currentLineAccNo) {
					if (adjRec) {
						invAdjRecId = nlapiSubmitRecord(adjRec);
						nlapiLogExecution('DEBUG', 'invAdjRecId Created is ', invAdjRecId);
						arrLineRecordId.push(shipReqDetailRslts[shipReqCount - 1].getId());
						for (var index = 0; index < arrLineRecordId.length; index++) {
							if (invAdjRecId) {
								nlapiSubmitField(shipReqDetailRslts[shipReqCount].getRecordType(), arrLineRecordId[index],
									['custrecord_shipping_details_shipstatus', 'custrecord_lirik_inv_adj_recs'], ['6', invAdjRecId]);
							}
						}
					}
					//Function Call to create new Inventory adjustment
					adjRec = createInvAdjustmentRecord(itemName, itemQty, invSerialNo, itemDesc, invAcct, invAdjType, invAdjLoc, invAdjSub, invAdjDept, soCust);
					arrLineRecordId = [];
					if (Number(shipReqCount - 1) >= 0) {
						arrLineRecordId.push(shipReqDetailRslts[shipReqCount].getId());
					}
					//nlapiLogExecution('DEBUG','adjRecId is ', adjRec);
				} else if (prevLineAccNo == currentLineAccNo && adjRec) {
					//Function Call to add new lines in inventory adjustment
					setDataOnInvAdjustment(adjRec, itemName, itemQty, invSerialNo, itemDesc, invAcct, invAdjType, invAdjLoc, invAdjSub, invAdjDept, soCust);
					//var invAdjRecId = nlapiSubmitRecord(adjRec);
					arrLineRecordId.push(shipReqDetailRslts[shipReqCount].getId());
				}

				// invAdjRecId = null;
				prevLineAccNo = currentLineAccNo;
			} /*else if (createStatus == 'Future') { // Create 0 value SO if Now/Future/Never field value is "Future".
				var soRec = createSalesOrderRec(itemName, itemQty, itemDesc, invAdjLoc, invAdjSub, invAdjDept, soCust);
				nlapiLogExecution('DEBUG', 'soRec is ', soRec);
				nlapiSubmitField(shipReqDetailRslts[shipReqCount].getRecordType(), shipReqDetailRslts[shipReqCount].getId(),
					['custrecord_lirik_inv_adj_recs'], [, soRec]);

			} else if (createStatus == 'Never') { // Mark ship request detail record inactive if Now/Future/Never field value is "Never".
				nlapiSubmitField(shipReqDetailRslts[shipReqCount].getRecordType(), shipReqDetailRslts[shipReqCount].getId(), 'isinactive', 'T');
			}*/
		}
		nlapiSetRedirectURL('RECORD', 'customrecord_shipping_request_record', shipReqId, false);
	} catch (ex) {
		nlapiLogExecution('ERROR', 'Exception in ' + shipReqId, ex);
	}
}

//Function to create new Inventory adjustment
var createInvAdjustmentRecord = function (itemName, itemQty, invSerialNo, itemDesc, invAcct, invAdjType, invAdjLoc, invAdjSub, invAdjDept, soCust) {

	if (itemName && itemQty && invAdjType && invAcct && invAdjLoc && invAdjSub && invAdjDept) {

		/** Start inventory adjustment record creation **/
		var invAdjRec = nlapiCreateRecord('inventoryadjustment');

		invAdjRec.setFieldValue('subsidiary', invAdjSub);
		invAdjRec.setFieldValue('department', invAdjDept);
		invAdjRec.setFieldValue('account', invAcct);
		if (soCust)
			invAdjRec.setFieldValue('customer', soCust);

		//Add line item on the inventory adjustment record
		invAdjRec.selectNewLineItem('inventory');
		invAdjRec.setCurrentLineItemValue('inventory', 'item', itemName);
		invAdjRec.setCurrentLineItemValue('inventory', 'location', invAdjLoc);
		if (invAdjType == '1') // If "Into Stock"
			invAdjRec.setCurrentLineItemValue('inventory', 'adjustqtyby', itemQty);
		if (invAdjType == '2') // If "Out of Stock"
			invAdjRec.setCurrentLineItemValue('inventory', 'adjustqtyby', '-' + itemQty);

		if (itemDesc)
			invAdjRec.setCurrentLineItemValue('inventory', 'description', itemDesc);

		if (invSerialNo) {
			var bodySubRecord = invAdjRec.createSubrecord('inventorydetail');
			for (var ctr = 1; ctr <= itemQty; ctr++) {
				//Here we are selecting a new line on the Inventory Assignment sublist on the subrecord
				bodySubRecord.selectNewLineItem('inventoryassignment');
				bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber',
					'shipRequest_' + ctr);
				bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
				//bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', 3);
				bodySubRecord.commitLineItem('inventoryassignment');
			}
			bodySubRecord.commit();
		}
		invAdjRec.commitLineItem('inventory');
		//var invAdjRecId = nlapiSubmitRecord(invAdjRec);
		//nlapiLogExecution('DEBUG','invAdjRecId is ', invAdjRecId);
		return invAdjRec;

		/** End inventory adjustment record creation **/
	}
};

var createSalesOrderRec = function (itemName, itemQty, itemDesc, invAdjLoc, invAdjSub, invAdjDept, soCust) {
	if (itemName && soCust && invAdjLoc && invAdjSub && invAdjDept) {

		/** Start Sales Order record creation **/
		var salesOrderRec = nlapiCreateRecord('salesorder');
		salesOrderRec.setFieldValue('entity', soCust);
		salesOrderRec.setFieldValue('subsidiary', invAdjSub);
		salesOrderRec.setFieldValue('department', invAdjDept);

		//Add line item on the inventory adjustment record
		salesOrderRec.selectNewLineItem('item');
		salesOrderRec.setCurrentLineItemValue('item', 'item', itemName);
		salesOrderRec.setCurrentLineItemValue('item', 'location', invAdjLoc);
		salesOrderRec.setCurrentLineItemValue('item', 'quantity', '0');
		salesOrderRec.setCurrentLineItemValue('item', 'amount', '0');

		if (itemDesc)
			salesOrderRec.setCurrentLineItemValue('item', 'description', itemDesc);

		salesOrderRec.commitLineItem('item');

		var salesOrderRecId = nlapiSubmitRecord(salesOrderRec);
		nlapiLogExecution('DEBUG', 'salesOrderRecId is ', invAdjRecId);
		return salesOrderRecId;
		/** End Sales Order record creation **/
	}
};

//Function to add new lines in inventory adjustment
function setDataOnInvAdjustment(adjRec, itemName, itemQty, invSerialNo, itemDesc, invAcct, invAdjType, invAdjLoc, invAdjSub, invAdjDept, soCust) {
	if (adjRec) {
		adjRec.setFieldValue('subsidiary', invAdjSub);
		adjRec.setFieldValue('department', invAdjDept);
		adjRec.setFieldValue('account', invAcct);
		if (soCust)
			adjRec.setFieldValue('customer', soCust);

		//Add line item on the inventory adjustment record
		adjRec.selectNewLineItem('inventory');
		adjRec.setCurrentLineItemValue('inventory', 'item', itemName);
		adjRec.setCurrentLineItemValue('inventory', 'location', invAdjLoc);
		if (invAdjType == '1') // If "Into Stock"
			adjRec.setCurrentLineItemValue('inventory', 'adjustqtyby', itemQty);
		if (invAdjType == '2') // If "Out of Stock"
			adjRec.setCurrentLineItemValue('inventory', 'adjustqtyby', '-' + itemQty);

		if (itemDesc)
			adjRec.setCurrentLineItemValue('inventory', 'description', itemDesc);

		if (invSerialNo) {
			var bodySubRecord = adjRec.createSubrecord('inventorydetail');
			for (var ctr = 1; ctr <= itemQty; ctr++) {
				//Here we are selecting a new line on the Inventory Assignment sublist on the subrecord
				bodySubRecord.selectNewLineItem('inventoryassignment');
				bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber',
					'shipRequest_' + ctr);
				bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'quantity', 1);
				//bodySubRecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', 3);
				bodySubRecord.commitLineItem('inventoryassignment');
			}
			bodySubRecord.commit();
		}
		adjRec.commitLineItem('inventory');
	}
}
