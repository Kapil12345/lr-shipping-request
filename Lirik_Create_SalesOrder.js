/**
 * File : Lirik_Create_SalesOrder.js (version 1.0)
 * Suitelet script to create Sales Order of 0 value on button click from shipping request record
 * Client: Liquid Robotics
 * Author: Kapil
 * Date: 21th Dec 2017
 * Company: Lirik Inc.
 *
 */

var loggedInRole = nlapiGetContext().getRoleId();

// Location is "LRI - Sunnyvale", Subsidiary is "Liquid Robotics"
var soLocation = nlapiGetContext().getSetting('SCRIPT', 'custscript_so_default_location'),
soSubsidiary = nlapiGetContext().getSetting('SCRIPT', 'custscript_so_default_subsidiary'),
mtlRole = nlapiGetContext().getSetting('SCRIPT', 'custscript_mtrl_manager'),
WarehouseManagerRole = nlapiGetContext().getSetting('SCRIPT', 'custscript_whouse_manager');

function processSalesOrder(request, response) {
try{
  var shipReqId = request.getParameter('shipreqid');
  makeSalesOrder(shipReqId);
  markInactive(shipReqId);
}
catch(e){
  nlapiLogExecution('ERROR','Error caught in main function-->',e);
}
}

function makeSalesOrder(shipReqId){
  try{
    // Searching related shipping request detail record
    var shipReqDetailFil = [],
    shipReqDetailCol = [];
    shipReqDetailFil.push(new nlobjSearchFilter('custrecord_now_future_never', null, 'is', '2'));
    var shipReqDetailRslts = searchChildShippingRecords(shipReqDetailFil,shipReqDetailCol,shipReqId);//, reqNo;

    if(shipReqDetailRslts){
      var salesOrderRec = nlapiCreateRecord('salesorder');
      for (var shipReqCount = 0; shipReqCount < shipReqDetailRslts.length; shipReqCount++) {
      //var createStatus = shipReqDetailRslts[shipReqCount].getText('custrecord_now_future_never'),
        var itemName = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_item'),
        itemQty = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_quantity'),
        itemDesc = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_description'),
        invSerialNo = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_inventorydet'),
        invAcct = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_invent_accno'),
        invAdjType = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_details_stock'),
        invAdjDept = shipReqDetailRslts[shipReqCount].getValue('custrecord_department', 'CUSTRECORD_SHIPPING_REQUEST_ID'),
        soCust = shipReqDetailRslts[shipReqCount].getValue('custrecord_shipping_customer', 'CUSTRECORD_SHIPPING_REQUEST_ID'),
        reqNo = shipReqDetailRslts[shipReqCount].getValue('custrecord_document_number', 'CUSTRECORD_SHIPPING_REQUEST_ID');

        var soRec = createSalesOrderRec(salesOrderRec,itemName, itemQty, itemDesc, soLocation, soSubsidiary, invAdjDept, soCust);
      }
      var salesOrderRecId = nlapiSubmitRecord(salesOrderRec,false,true);
      if(salesOrderRecId){
        for(var shipChildIndex = 0; shipChildIndex < shipReqDetailRslts.length; shipChildIndex++){
            nlapiSubmitField(shipReqDetailRslts[shipChildIndex].getRecordType(), shipReqDetailRslts[shipChildIndex].getId(),
              ['custrecord_lirik_inv_adj_recs'], salesOrderRecId);
        }
        if(nlapiGetRole() == mtlRole){//
        var salesOrderTranId = nlapiLookUpField('salesorder',salesOrderRecId,'tranid');
        var subject = 'MT # '+checkNull(reqNo)+' Sales Order #: '+salesOrderTranId+'';
        var body = 'Please find below details<br/><table><tr><td>MT#: </td><td>'+checkNull(reqNo)+'</td></tr><tr><td>Sales Order: </td><td>'+checkNull(salesOrderTranId)+'</td></tr></table>';
        var getWarehouseManagers = searchWarehouseManagers(WarehouseManagerRole);
        if(getWarehouseManagers)
          nlapiSendEmail(nlapiGetUser(),getWarehouseManagers[0].getValue('email'),subject,body);
        }
      }
    }
    nlapiSetRedirectURL('RECORD', 'customrecord_shipping_request_record', shipReqId, false);
  }
  catch(e){
    nlapiLogExecution('ERROR','Error in create Sales Order-->',e);
  }
}
var createSalesOrderRec = function (salesOrderRec,itemName, itemQty, itemDesc, soLocation, soSubsidiary, invAdjDept, soCust) {
	if (salesOrderRec && itemName && soCust && soLocation && soSubsidiary && invAdjDept) {

		/** Start Sales Order record creation **/

		salesOrderRec.setFieldValue('entity', soCust);
		salesOrderRec.setFieldValue('subsidiary', soSubsidiary);
		salesOrderRec.setFieldValue('department', invAdjDept);

		//Add line item on the inventory adjustment record
		salesOrderRec.selectNewLineItem('item');
		salesOrderRec.setCurrentLineItemValue('item', 'item', itemName);
		salesOrderRec.setCurrentLineItemValue('item', 'location', soLocation);
		salesOrderRec.setCurrentLineItemValue('item', 'quantity', '0');
		salesOrderRec.setCurrentLineItemValue('item', 'amount', '0');

		if (itemDesc)
			salesOrderRec.setCurrentLineItemValue('item', 'description', itemDesc);

		salesOrderRec.commitLineItem('item');


		// nlapiLogExecution('DEBUG', 'salesOrderRecId is ', invAdjRecId);
		// return salesOrderRecId;
		/** End Sales Order record creation **/
	}
};

function searchChildShippingRecords(shipReqDetailFil,shipReqDetailCol,shipReqId){
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shipReqId));
  shipReqDetailFil.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', 'T'));
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_lirik_inv_adj_recs', null, 'anyof', '@NONE@'));
  //if (loggedInRole == 'customrole1027') // Role is "LRI Warehouse Manager"
  //shipReqDetailFil.push(new nlobjSearchFilter('custrecord_shipping_details_inventoryadj', null, 'is', 'T'));

  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_item'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_stock'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_invent_accno'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_quantity'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_now_future_never'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_details_description'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_department', 'CUSTRECORD_SHIPPING_REQUEST_ID'));
  shipReqDetailCol.push(new nlobjSearchColumn('custrecord_shipping_customer', 'CUSTRECORD_SHIPPING_REQUEST_ID'));

  var shipReqDetailRslts = nlapiSearchRecord('customrecord_shipping_request_details', null, shipReqDetailFil, shipReqDetailCol);
  return shipReqDetailRslts;
}

function markInactive(shipReqId){
  // Searching related shipping request detail record
  var shipReqDetailFil = [],
  shipReqDetailCol = [];
  shipReqDetailFil.push(new nlobjSearchFilter('custrecord_now_future_never', null, 'is', '3'));
  var shipReqDetailRslts = searchChildShippingRecords(shipReqDetailFil,shipReqDetailCol,shipReqId);

  if(shipReqDetailRslts){
    for (var shipReqCount = 0; shipReqCount < shipReqDetailRslts.length; shipReqCount++) {
      nlapiSubmitField(shipReqDetailRslts[shipReqCount].getRecordType(), shipReqDetailRslts[shipReqCount].getId(),
        ['isinactive'], 'T');
    }
  }
  nlapiSetRedirectURL('RECORD', 'customrecord_shipping_request_record', shipReqId, false);
}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

function searchWarehouseManagers(WarehouseManagerRole){
var wareHouseManagerColumns = [];
var wareHouseManagerFilters = [];
wareHouseManagerFilters.push(new nlobjSearchFilter('entityid',null ,'contains', 'Anthony Truong'),new nlobjSearchFilter('email', null,'isnotempty'), nlobjSearchFilter('role', null, 'anyof', WarehouseManagerRole));
wareHouseManagerColumns.push(new nlobjSearchColumn('email'));
var searchWarehouseManagersResult = nlapiSearchRecord('employee', null, wareHouseManagerFilters, wareHouseManagerColumns);
if (searchWarehouseManagersResult) {
  return searchWarehouseManagersResult;
}
}
