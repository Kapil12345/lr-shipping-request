/*******************************************************************
 *
 *
 * Name: Lirik_Edit_ShippingRequest_Screen.js
 * Script Type: Suitelet
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This screen is used to edit the request submitted by requester from MT Manager role.
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function editRequest(request, response) {
  try {
    var LRIMTManagerRole = request.getParameter('custparam_mtrole');
    var WarehouseManagerRole = request.getParameter('custparam_warehouserole');
    var shippingRequestId = request.getParameter('custparam_requestid');
    if (request.getMethod() == 'GET') {
      nlapiLogExecution('DEBUG', 'GET shippingRequestId-->', shippingRequestId);
      // var LRIMTManagerRole = request.getParameter('custparam_mtrole');
      // var WarehouseManagerRole = request.getParameter('custparam_warehouserole');
      // var shippingRequestId = request.getParameter('custparam_requestid');
      var currentRole = nlapiGetRole();
      var editRequestForm = nlapiCreateForm('Edit Shipping Request');
      //Set client script
      editRequestForm.setScript('customscript_lirik_cs_valid_mangr_screen');
      if ((currentRole == '3' || currentRole == LRIMTManagerRole) && shippingRequestId) {
        var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shippingRequestId);
        var shippingRequestIdFld = editRequestForm.addField('custpage_shiprequestid', 'text', '', 'employee').setDisplayType('hidden');
        if (shippingRequestId)
          shippingRequestIdFld.setDefaultValue(shippingRequestId);
        var requestor = editRequestForm.addField('custpage_requestor', 'select', 'Requestor', 'employee').setDisplayType('inline');
        requestor.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_requester')));
        var department = editRequestForm.addField('custpage_department', 'select', 'Department', 'department').setDisplaySize('420');
        department.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_department')));
        var createDate = editRequestForm.addField('custpage_date', 'date', 'Creation Date').setDisplayType('inline');
        createDate.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_creation_date')));
        var documentNo = editRequestForm.addField('custpage_docno', 'integer', 'Document #').setDisplayType('inline');
        documentNo.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_document_number')));
        var trantype = editRequestForm.addField('custpage_trantype', 'select', 'Transaction Type', 'customlist_shipping_transaction_type').setMandatory(true);
        trantype.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_transaction_type')));
        var reqComments = editRequestForm.addField('custpage_reqcomments', 'textarea', 'Requestor Comments');
        reqComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_requesters_comments')));
        var logMrgComments = editRequestForm.addField('custpage_logmrgcomm', 'textarea', 'Logistics Manager Comments');
        logMrgComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_logistics_managers_comments')));
        var matMrgComments = editRequestForm.addField('custpage_matmrgcomm', 'textarea', 'Material Manager Comments').setLayoutType('normal', 'startcol');
        matMrgComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_materials_managers_comments')));
        var customerFld = editRequestForm.addField('custpage_customer', 'select', 'Customer', 'customer');
        customerFld.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_shipping_customer')));
        var employeeFld = editRequestForm.addField('custpage_employee', 'select', 'Employee', 'employee');
        employeeFld.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_shipping_employee')));
        var shipToAdd = editRequestForm.addField('custpage_shiptoadd', 'textarea', 'Ship To Address');
        shipToAdd.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address')));
        var shipFromAdd = editRequestForm.addField('custpage_shipfromadd', 'textarea', 'Ship From Address');
        shipFromAdd.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_ship_from_address')));
        var approver = editRequestForm.addField('custpage_approver', 'select', 'Approver', 'employee'); //.setDisplayType('inline');
        approver.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_approver')));
        var isApproved = editRequestForm.addField('custpage_isapproved', 'select', 'Is Approved ?', 'customlist_shipment_request_list');
        isApproved.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_is_approved')));
        //setApproverList(approver);
        var totalamount = editRequestForm.addField('custpage_total', 'currency', 'Total Amount').setDisplayType('inline').setLayoutType('normal', 'startcol');
        totalamount.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_total')));
        var projectLengthField = editRequestForm.addField('custpage_project_length','textarea','Project Length').setDisplayType('hidden');
        //Sublist code for MT Manager
        var getShippingDetailsRecord = searchMTShippingDetails(shippingRequestId);
        nlapiLogExecution('DEBUG', 'getShippingDetailsRecord', JSON.stringify(getShippingDetailsRecord));
        var details = editRequestForm.addSubList('custpage_details', 'list', 'Items');
        var detailId = details.addField('custpage_detailid', 'text', '').setDisplayType('hidden');
        var readyforInvAdj = details.addField('custpage_ready_forinvadj', 'checkbox', 'Ready for Inv Adj').setDisplayType('entry');
        var lineSeq = details.addField('custpage_lineseq', 'text', 'Line Sequence #'); //.setDisplayType('entry');
        var nowFutNever = details.addField('custpage_nowfutnever', 'select', 'Now/Future/Never', 'customlist_shipping_request_mtdone').setDisplayType('entry');
        var adjStock = details.addField('custpage_adjuststock', 'select', 'Adjust Stock?(Yes/No)', 'customlist_shipment_request_list').setDisplayType('entry');
        var itemDisplay = details.addField('custpage_item', 'select', 'Item', 'item').setDisplayType('entry');
        var itemDesc = details.addField('custpage_des', 'textarea', 'Description').setDisplayType('entry');
        var itemQty = details.addField('custpage_qty', 'integer', 'Quantity').setDisplayType('entry');
        var invDetails = details.addField('custpage_invdetail', 'text', 'Inventory Detail (Serial #)').setDisplayType('entry');
        var invAdjAccountNo = details.addField('custpage_invadjaccno', 'select', 'Inventory Adj Acc No','account').setDisplayType('entry').setMandatory(true);
        var intoOutStock = details.addField('custpage_stock', 'select', 'Into or Out Of Stock', 'customlist_shipment_stock_list').setDisplayType('entry');
        var delvDate = details.addField('custpage_deliveryddate', 'date', 'Requested Delivery Date').setDisplayType('entry');
        var reqShipDate = details.addField('custpage_shipdate', 'date', 'Requested Ship Date').setDisplayType('entry')
        var project = details.addField('custpage_project', 'select', 'Project #', 'job').setDisplayType('entry') //.setMandatory(true);
        var priority = details.addField('custpage_priority', 'text', 'Expedite/Priority needs').setDisplayType('entry');
        var poRMA = details.addField('custpage_po', 'text', 'PO/RMA/SOD/SODW').setDisplayType('entry');
        var itar = details.addField('custpage_itar', 'select', 'ITAR/EAR', 'customlist_shipment_request_list').setDisplayType('entry') //.setMandatory(true);
        var hazmat = details.addField('custpage_hazmat', 'select', 'Hazmat Status(Batteries)', 'customlist_shipment_request_list').setDisplayType('entry') //.setMandatory(true);
        var carnet = details.addField('custpage_carnet', 'select', 'Carnet Desired', 'customlist_shipment_request_list').setDisplayType('entry') //.setMandatory(true);
        var carnetDetails = details.addField('custpage_carnetdetails', 'textarea', 'Carnet Details').setDisplayType('entry');
        var emailVal = details.addField('custpage_email', 'text', 'Email(s) comma seperated').setDisplayType('entry') //.setMandatory(true);
        var trackingDetails = details.addField('custpage_tracking', 'text', 'Tracking #').setDisplayType('entry');
        var shipmentDate = details.addField('custpage_shipmentdate', 'date', 'Shipment Date').setDisplayType('entry');
        var shipmentStatus = details.addField('custpage_shipmentstatus', 'select', 'Shipment Status', 'customlist_shipment_status_list').setDisplayType('entry');
        var itemCost = details.addField('custpage_itemcost', 'currency', 'Item Cost').setDisplayType('entry');
        var comments = details.addField('custpage_comments', 'textarea', 'Comments').setDisplayType('entry');
        var relatedTransactions = details.addField('custpage_related_transactions', 'select', 'Related Transactions','transaction').setDisplayType('entry');

        //Filling Detail Line for MT Manager
        if (getShippingDetailsRecord) {
          for (var mtDetailIndex = 0; mtDetailIndex < getShippingDetailsRecord.length; mtDetailIndex++) {
            details.setLineItemValue('custpage_detailid', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getId()));
            details.setLineItemValue('custpage_ready_forinvadj', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_inventoryadj')));
            details.setLineItemValue('custpage_shipdate', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_shipdate')));
            details.setLineItemValue('custpage_lineseq', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_lineseq')));
            details.setLineItemValue('custpage_nowfutnever', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_now_future_never')));
            details.setLineItemValue('custpage_adjuststock', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_adjuststock')));
            details.setLineItemValue('custpage_item', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_item')));
            details.setLineItemValue('custpage_des', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_description')));
            details.setLineItemValue('custpage_qty', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_quantity')));
            details.setLineItemValue('custpage_invdetail', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_inventorydet')));
            details.setLineItemValue('custpage_invadjaccno', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_invent_accno')));
            details.setLineItemValue('custpage_stock', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_stock')));
            details.setLineItemValue('custpage_project', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_project')));
            //priority.setDefaultValue(checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_lineseq')));
            details.setLineItemValue('custpage_email', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_email')));
            details.setLineItemValue('custpage_shipmentstatus', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_shipstatus')));
            details.setLineItemValue('custpage_itemcost', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_itemcost')));
            details.setLineItemValue('custpage_comments', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_comments')));
            details.setLineItemValue('custpage_related_transactions', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_lirik_inv_adj_recs')));
          }
        }
      }
      editRequestForm.addSubmitButton('Submit');
      response.writePage(editRequestForm);
    } else if (request.getMethod() == 'POST') {
      nlapiLogExecution('DEBUG', 'POST shippingRequestId-->', request.getParameter('custpage_shiprequestid'));
      nlapiLogExecution('DEBUG', 'POST transaction type-->', request.getParameter('custpage_trantype'));
      if (request.getParameter('custpage_shiprequestid')) {
        var custRecord = nlapiLoadRecord('customrecord_shipping_request_record', request.getParameter('custpage_shiprequestid'));
        //nlapiLogExecution('DEBUG','GET shippingRequestId-->',shippingRequestId);
        //custRecord.setFieldValue('custrecord_requester', checkNull(request.getParameter('custpage_requestor')));
        var bodyFieldMap = {
          fields: {
            custrecord_requester: 'custpage_requestor',
            custrecord_transaction_type: 'custpage_trantype',
            custrecord_department: 'custpage_department',
            custrecord_materials_managers_comments: 'custpage_matmrgcomm',
            custrecord_logistics_managers_comments: 'custpage_logmrgcomm',
            custrecord_creation_date: 'custpage_date',
            custrecord_document_number: 'custpage_docno',
            custrecord_requesters_comments: 'custpage_reqcomments',
            custrecord_shipping_customer: 'custpage_customer',
            custrecord_shipping_employee: 'custpage_employee',
            custrecord_ship_to_address: 'custpage_shiptoadd',
            custrecord_ship_from_address: 'custpage_shipfromadd',
            custrecord_approver: 'custpage_approver',
            custrecord_is_approved: 'custpage_isapproved',
            custrecord_total: 'custpage_total'
          }
        };
        var bodyKeysFields = Object.keys(bodyFieldMap.fields);
        for (var fieldIndex = 0; fieldIndex < bodyKeysFields.length; fieldIndex++) {
          custRecord.setFieldValue(bodyKeysFields[fieldIndex], checkNull(request.getParameter(bodyFieldMap.fields[bodyKeysFields[fieldIndex]])));
        }
        var shippingRecId = nlapiSubmitRecord(custRecord);

        var lineFieldMap = {
          //sublistId: 'recmachcustrecord_shipping_request_id',
          fields: {
            custrecord_shipping_details_lineseq: 'custpage_lineseq',
            custrecord_shipping_details_inventoryadj:'custpage_ready_forinvadj',
            custrecord_shipping_details_adjuststock: 'custpage_adjuststock',
            custrecord_now_future_never: 'custpage_nowfutnever',
            custrecord_shipping_details_item: 'custpage_item',
            custrecord_shipping_details_description: 'custpage_des',
            custrecord_shipping_details_quantity: 'custpage_qty',
            custrecord_shipping_details_stock: 'custpage_stock',
            custrecord_shipping_details_deldate: 'custpage_deliveryddate',
            custrecord_shipping_details_shipdate: 'custpage_shipdate',
            custrecord_shipping_details_project: 'custpage_project',
            custrecord_shipping_details_priorityneed: 'custpage_priority',
            custrecord_shipping_details_po: 'custpage_po',
            custrecord_shipping_details_itar: 'custpage_itar',
            custrecord_shipping_details_hazmat: 'custpage_hazmat',
            custrecord_shipping_details_carnet: 'custpage_carnet',
            custrecord_shipping_details_carnetdetail: 'custpage_carnetdetails',
            custrecord_shipping_details_email: 'custpage_email',
            custrecord_shipping_details_invent_accno: 'custpage_invadjaccno',
            //custrecord_shipping_details_tracking: 'custpage_tracking',
            custrecord_shipping_details_shipmentdate: 'custpage_shipmentdate',
            custrecord_shipping_details_shipstatus: 'custpage_shipmentstatus',
            custrecord_shipping_details_itemcost: 'custpage_itemcost',
            custrecord_shipping_details_comments: 'custpage_comments',
            custrecord_lirik_inv_adj_recs: 'custpage_related_transactions'
          }
        };
        var keysFields = Object.keys(lineFieldMap.fields);

        for (var i = 1; request.getLineItemCount('custpage_details') && i <= request.getLineItemCount('custpage_details'); i++) {
          var custDetailRecord = nlapiLoadRecord('customrecord_shipping_request_details', request.getLineItemValue('custpage_details', 'custpage_detailid', i));
          //custRecord.selectNewLineItem('recmachcustrecord_shipping_request_id');
          for (var lineFieldIndex = 0; lineFieldIndex < keysFields.length; lineFieldIndex++) {
            //custRecord.setCurrentLineItemValue(lineFieldMap.sublistId, keysFields[lineFieldIndex], checkNull(request.getLineItemValue('custpage_details', lineFieldMap.fields[keysFields[lineFieldIndex]], i)));
            nlapiLogExecution('DEBUG', 'editRequest', lineFieldMap.fields[keysFields[lineFieldIndex]] + '::' + checkNull(request.getLineItemValue('custpage_details',lineFieldMap.fields[keysFields[lineFieldIndex]],i)));
            custDetailRecord.setFieldValue(keysFields[lineFieldIndex], checkNull(request.getLineItemValue('custpage_details',lineFieldMap.fields[keysFields[lineFieldIndex]],i)));

          }
          var shippingRecDetailsId = nlapiSubmitRecord(custDetailRecord);
        }

        response.write('<html><body><script type="text/javascript">window.onunload = function(e){window.opener.location.reload();}; window.close();</script></body></html>');
      }
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error Caught in Edit Request Suitelet-->', e);
  }
}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

function searchMTShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', '1'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_deldate'),new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;

}
