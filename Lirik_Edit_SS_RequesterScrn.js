/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       15 Jan 2017     Kapil            This script is used to provide the edit functionality to requester
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
 function editShippingRequest(request, response) {
   try {
     var LRIMTManagerRole = request.getParameter('custparam_mtrole');
     var WarehouseManagerRole = request.getParameter('custparam_warehouserole');
     var empCenterRole = request.getParameter('custparam_emprole');
     var shippingRequestId = request.getParameter('custparam_requestid');
     if (request.getMethod() == 'GET') {
       nlapiLogExecution('DEBUG', 'GET shippingRequestId-->', shippingRequestId);
       // var LRIMTManagerRole = request.getParameter('custparam_mtrole');
       // var WarehouseManagerRole = request.getParameter('custparam_warehouserole');
       // var shippingRequestId = request.getParameter('custparam_requestid');
       var currentRole = nlapiGetRole();
       var editRequestForm = nlapiCreateForm('Edit Shipping Request');
       //Set client script
       editRequestForm.setScript('customscript_cs_validateshippingdetails');
       if ((currentRole == '3' || currentRole == empCenterRole) && shippingRequestId) {
         var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shippingRequestId);
         var shippingRequestIdFld = editRequestForm.addField('custpage_shiprequestid', 'text', '', 'employee').setDisplayType('hidden');
         if (shippingRequestId)
           shippingRequestIdFld.setDefaultValue(shippingRequestId);
         var requestor = editRequestForm.addField('custpage_requestor', 'select', 'Requestor', 'employee').setDisplayType('inline');
         requestor.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_requester')));
         var department = editRequestForm.addField('custpage_department', 'select', 'Department', 'department').setDisplaySize('420');
         department.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_department')));
         var createDate = editRequestForm.addField('custpage_date', 'date', 'Creation Date').setDisplayType('inline');
         createDate.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_creation_date')));
         var documentNo = editRequestForm.addField('custpage_docno', 'integer', 'Document #').setDisplayType('inline');
         documentNo.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_document_number')));
         var trantype = editRequestForm.addField('custpage_trantype', 'select', 'Transaction Type', 'customlist_shipping_transaction_type').setMandatory(true);
         trantype.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_transaction_type')));
         var reqComments = editRequestForm.addField('custpage_reqcomments', 'textarea', 'Requestor Comments');
         reqComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_requesters_comments')));
         var logMrgComments = editRequestForm.addField('custpage_logmrgcomm', 'textarea', 'Logistics Manager Comments');
         logMrgComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_logistics_managers_comments')));
         var matMrgComments = editRequestForm.addField('custpage_matmrgcomm', 'textarea', 'Material Manager Comments').setLayoutType('normal', 'startcol');
         matMrgComments.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_materials_managers_comments')));
         var customerFld = editRequestForm.addField('custpage_customer', 'select', 'Customer', 'customer');
         customerFld.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_shipping_customer')));
         var employeeFld = editRequestForm.addField('custpage_employee', 'select', 'Employee', 'employee');
         employeeFld.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_shipping_employee')));
         var shipToAdd = editRequestForm.addField('custpage_shiptoadd', 'textarea', 'Ship To Address');
         shipToAdd.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address')));
         var shipFromAdd = editRequestForm.addField('custpage_shipfromadd', 'textarea', 'Ship From Address');
         shipFromAdd.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_ship_from_address')));
         var approver = editRequestForm.addField('custpage_approver', 'select', 'Approver', 'employee').setMandatory(true);; //.setDisplayType('inline');
         approver.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_approver')));
         var isApproved = editRequestForm.addField('custpage_isapproved', 'select', 'Is Approved ?', 'customlist_shipment_request_list');
         isApproved.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_is_approved')));
         //setApproverList(approver);
         var totalamount = editRequestForm.addField('custpage_total', 'currency', 'Total Amount').setDisplayType('inline').setLayoutType('normal', 'startcol');
         totalamount.setDefaultValue(checkNull(shipReqObj.getFieldValue('custrecord_total')));
         var projectLengthField = editRequestForm.addField('custpage_project_length','longtext','Project Length').setDisplayType('hidden');
         //Sublist code for MT Manager
         var getShippingDetailsRecord = searchMTShippingDetails(shippingRequestId);
         nlapiLogExecution('DEBUG', 'getShippingDetailsRecord', JSON.stringify(getShippingDetailsRecord));
         var details = editRequestForm.addSubList('custpage_details', 'inlineeditor', 'Items');
         var detailId = details.addField('custpage_detailid', 'text', '').setDisplayType('hidden');
         var readyforInvAdj = details.addField('custpage_ready_forinvadj', 'checkbox', 'Ready for Inv Adj').setDisplayType('hidden');
         var lineSeq = details.addField('custpage_lineseq', 'text', 'Line Sequence #'); //.setDisplayType('entry');
         var nowFutNever = details.addField('custpage_nowfutnever', 'select', 'Now/Future/Never', 'customlist_shipping_request_mtdone');
         var adjStock = details.addField('custpage_adjuststock', 'select', 'Adjust Stock?(Yes/No)', 'customlist_shipment_request_list');
         var itemDisplay = details.addField('custpage_item', 'select', 'Item', 'item');
         var itemDesc = details.addField('custpage_des', 'textarea', 'Description');
         var itemQty = details.addField('custpage_qty', 'integer', 'Quantity');
         var invDetails = details.addField('custpage_invdetail', 'text', 'Inventory Detail (Serial #)');
         var invAdjAccountNo = details.addField('custpage_invadjaccno', 'select', 'Inventory Adj Acc No','account');
         var intoOutStock = details.addField('custpage_stock', 'select', 'Into or Out Of Stock', 'customlist_shipment_stock_list');
         var delvDate = details.addField('custpage_deliveryddate', 'date', 'Requested Delivery Date');
         var reqShipDate = details.addField('custpage_shipdate', 'date', 'Requested Ship Date')
         var project = details.addField('custpage_project', 'select', 'Project #', '').setMandatory(true);
         var priority = details.addField('custpage_priority', 'text', 'Expedite/Priority needs').setMandatory(true);
         var poRMA = details.addField('custpage_po', 'text', 'PO/RMA/SOD/SODW');
         var itar = details.addField('custpage_itar', 'select', 'ITAR/EAR', 'customlist_shipment_request_list').setMandatory(true);
         var hazmat = details.addField('custpage_hazmat', 'select', 'Hazmat Status(Batteries)', 'customlist_shipment_request_list').setMandatory(true);
         var carnet = details.addField('custpage_carnet', 'select', 'Carnet Desired', 'customlist_shipment_request_list').setMandatory(true);
         var carnetDetails = details.addField('custpage_carnetdetails', 'textarea', 'Carnet Details');
         var emailVal = details.addField('custpage_email', 'text', 'Email(s) comma seperated').setMandatory(true);
         var trackingDetails = details.addField('custpage_tracking', 'text', 'Tracking #');
         var shipmentDate = details.addField('custpage_shipmentdate', 'date', 'Shipment Date');
         var shipmentStatus = details.addField('custpage_shipmentstatus', 'select', 'Shipment Status', 'customlist_shipment_status_list');
         var itemCost = details.addField('custpage_itemcost', 'currency', 'Item Cost');
         var comments = details.addField('custpage_comments', 'textarea', 'Comments').setMandatory(true);
         var relatedTransactions = details.addField('custpage_related_transactions', 'select', 'Related Transactions','transaction').setDisplayType('hidden');

         //Filling Detail Line for MT Manager
         if (getShippingDetailsRecord) {
           for (var mtDetailIndex = 0; mtDetailIndex < getShippingDetailsRecord.length; mtDetailIndex++) {
             details.setLineItemValue('custpage_detailid', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getId()));
             details.setLineItemValue('custpage_ready_forinvadj', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_inventoryadj')));
             details.setLineItemValue('custpage_shipdate', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_shipdate')));
             details.setLineItemValue('custpage_lineseq', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_lineseq')));
             details.setLineItemValue('custpage_nowfutnever', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_now_future_never')));
             details.setLineItemValue('custpage_adjuststock', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_adjuststock')));
             details.setLineItemValue('custpage_item', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_item')));
             details.setLineItemValue('custpage_des', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_description')));
             details.setLineItemValue('custpage_qty', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_quantity')));
             details.setLineItemValue('custpage_invdetail', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_inventorydet')));
             details.setLineItemValue('custpage_invadjaccno', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_invent_accno')));
             details.setLineItemValue('custpage_stock', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_stock')));
             details.setLineItemValue('custpage_deliveryddate', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_deldate')));
             details.setLineItemValue('custpage_project', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_project')));
             details.setLineItemValue('custpage_priority', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_priorityneed')));
             details.setLineItemValue('custpage_itar', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_itar')));
             details.setLineItemValue('custpage_hazmat', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_hazmat')));
             details.setLineItemValue('custpage_carnet', parseInt(mtDetailIndex) + 1,checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_carnet')));
             details.setLineItemValue('custpage_carnetdetails', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_carnetdetail')));
             details.setLineItemValue('custpage_tracking', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_tracking')));
             details.setLineItemValue('custpage_shipmentdate', parseInt(mtDetailIndex) + 1,checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_shipmentdate')));
             //priority.setDefaultValue(checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_lineseq')));
             details.setLineItemValue('custpage_email', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_email')));
             details.setLineItemValue('custpage_shipmentstatus', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_shipstatus')));
             details.setLineItemValue('custpage_itemcost', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_itemcost')));
             details.setLineItemValue('custpage_comments', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_shipping_details_comments')));
             details.setLineItemValue('custpage_related_transactions', parseInt(mtDetailIndex) + 1, checkNull(getShippingDetailsRecord[mtDetailIndex].getValue('custrecord_lirik_inv_adj_recs')));
           }
         }
       }
       editRequestForm.addSubmitButton('Save');
       response.writePage(editRequestForm);
     } else if (request.getMethod() == 'POST') {
       nlapiLogExecution('DEBUG', 'POST shippingRequestId-->', request.getParameter('custpage_shiprequestid'));
       nlapiLogExecution('DEBUG', 'POST transaction type-->', request.getParameter('custpage_trantype'));
       if (request.getParameter('custpage_shiprequestid')) {
         var custRecord = nlapiLoadRecord('customrecord_shipping_request_record', request.getParameter('custpage_shiprequestid'));
         //nlapiLogExecution('DEBUG','GET shippingRequestId-->',shippingRequestId);
         //custRecord.setFieldValue('custrecord_requester', checkNull(request.getParameter('custpage_requestor')));
         var bodyFieldMap = {
           fields: {
             custrecord_requester: 'custpage_requestor',
             custrecord_transaction_type: 'custpage_trantype',
             custrecord_department: 'custpage_department',
             custrecord_materials_managers_comments: 'custpage_matmrgcomm',
             custrecord_logistics_managers_comments: 'custpage_logmrgcomm',
             custrecord_creation_date: 'custpage_date',
             custrecord_document_number: 'custpage_docno',
             custrecord_requesters_comments: 'custpage_reqcomments',
             custrecord_shipping_customer: 'custpage_customer',
             custrecord_shipping_employee: 'custpage_employee',
             custrecord_ship_to_address: 'custpage_shiptoadd',
             custrecord_ship_from_address: 'custpage_shipfromadd',
             custrecord_approver: 'custpage_approver',
             custrecord_is_approved: 'custpage_isapproved',
             custrecord_total: 'custpage_total'
           }
         };
         var bodyKeysFields = Object.keys(bodyFieldMap.fields);
         for (var fieldIndex = 0; fieldIndex < bodyKeysFields.length; fieldIndex++) {
           custRecord.setFieldValue(bodyKeysFields[fieldIndex], checkNull(request.getParameter(bodyFieldMap.fields[bodyKeysFields[fieldIndex]])));
         }
         var shippingRecId = nlapiSubmitRecord(custRecord);

         var lineFieldMap = {
           //sublistId: 'recmachcustrecord_shipping_request_id',
           fields: {
             custrecord_shipping_details_lineseq: 'custpage_lineseq',
             custrecord_shipping_details_inventoryadj:'custpage_ready_forinvadj',
             custrecord_shipping_details_adjuststock: 'custpage_adjuststock',
             custrecord_now_future_never: 'custpage_nowfutnever',
             custrecord_shipping_details_item: 'custpage_item',
             custrecord_shipping_details_description: 'custpage_des',
             custrecord_shipping_details_quantity: 'custpage_qty',
             custrecord_shipping_details_stock: 'custpage_stock',
             custrecord_shipping_details_deldate: 'custpage_deliveryddate',
             custrecord_shipping_details_shipdate: 'custpage_shipdate',
             custrecord_shipping_details_project: 'custpage_project',
             custrecord_shipping_details_priorityneed: 'custpage_priority',
             custrecord_shipping_details_po: 'custpage_po',
             custrecord_shipping_details_itar: 'custpage_itar',
             custrecord_shipping_details_hazmat: 'custpage_hazmat',
             custrecord_shipping_details_carnet: 'custpage_carnet',
             custrecord_shipping_details_carnetdetail: 'custpage_carnetdetails',
             custrecord_shipping_details_email: 'custpage_email',
             custrecord_shipping_details_invent_accno: 'custpage_invadjaccno',
             //custrecord_shipping_details_tracking: 'custpage_tracking',
             custrecord_shipping_details_shipmentdate: 'custpage_shipmentdate',
             custrecord_shipping_details_shipstatus: 'custpage_shipmentstatus',
             custrecord_shipping_details_itemcost: 'custpage_itemcost',
             custrecord_shipping_details_comments: 'custpage_comments',
             custrecord_lirik_inv_adj_recs: 'custpage_related_transactions'
           }
         };
         var keysFields = Object.keys(lineFieldMap.fields);

         for (var i = 1; request.getLineItemCount('custpage_details') && i <= request.getLineItemCount('custpage_details'); i++) {
           var custDetailRecord = null;
           if(request.getLineItemValue('custpage_details', 'custpage_detailid', i)){
              custDetailRecord = nlapiLoadRecord('customrecord_shipping_request_details', request.getLineItemValue('custpage_details', 'custpage_detailid', i));
              custDetailRecord.setFieldValue('custrecord_shipping_request_id', checkNull(shippingRecId));
              //custDetailRecord.setFieldText('custrecord_shipping_request_id',shippingRecId);
           }
           else{
             custDetailRecord = nlapiCreateRecord('customrecord_shipping_request_details');
             //custDetailRecord.setFieldText('custrecord_shipping_request_id',shippingRecId);
             custDetailRecord.setFieldValue('custrecord_shipping_request_id', checkNull(shippingRecId));
           }

           //custRecord.selectNewLineItem('recmachcustrecord_shipping_request_id');
           for (var lineFieldIndex = 0; lineFieldIndex < keysFields.length; lineFieldIndex++) {
             //custRecord.setCurrentLineItemValue(lineFieldMap.sublistId, keysFields[lineFieldIndex], checkNull(request.getLineItemValue('custpage_details', lineFieldMap.fields[keysFields[lineFieldIndex]], i)));
             nlapiLogExecution('DEBUG', 'editRequest', lineFieldMap.fields[keysFields[lineFieldIndex]] + '::' + checkNull(request.getLineItemValue('custpage_details',lineFieldMap.fields[keysFields[lineFieldIndex]],i)));
             custDetailRecord.setFieldValue(keysFields[lineFieldIndex], checkNull(request.getLineItemValue('custpage_details',lineFieldMap.fields[keysFields[lineFieldIndex]],i)));

           }
           var shippingRecDetailsId = nlapiSubmitRecord(custDetailRecord);
         }

         response.write('<html><body><script type="text/javascript">window.onunload = function(e){window.opener.location.reload();}; window.close();</script></body></html>');
       }
       if(request.getParameter('custpage_approver') && request.getParameter('custpage_requestor')){
         var approverEmail = nlapiLookupField('employee',request.getParameter('custpage_approver'),'email');
         var email_ShippingToArr = [],email_ToArr = [];
         //email_ShippingToArr.push(approverEmail);
         //email_ToArr.push(approverEmail);
         var flag=false;
         for(var reqIndex=1; reqIndex<request.getLineItemCount('custpage_details'); reqIndex++){
           var adjustStock = request.getLineItemValue('custpage_details','custpage_adjuststock',reqIndex);
           if(adjustStock == '1'){
             flag=true;
             // var emailMTManagers = getMtManagers();
             // var emailTo = request.getLineItemValue('custpage_details','custpage_email',reqIndex);
             // if(emailTo)
             //   email_ToArr.push(emailTo);
             // if(emailMTManagers){
             //   for(var MTIndex=0; MTIndex<emailMTManagers.length; MTIndex++){
             //     email_MTToArr.push(emailMTManagers[MTIndex].getValue('email'));
             //   }
             // }// TODO: create attachment to be sent to approver
             // var attachmentMT = emailAttachment(adjustStock);
           }
           /*else if(adjustStock == '2') {
             var emailTo = request.getLineItemValue('custpage_details','custpage_email',reqIndex);
             if(emailTo)
               email_ShippingToArr.push(emailTo);
             var attachmentShipping = emailAttachment(adjustStock);
             var subject = 'A Shipping Request is pending for your approval', body='Shipping Request is pending for Approval';
             nlapiSendEmail(request.getParameter('custpage_requestor'),request.getParameter('custpage_approver'),subject,body,null,null,null,attachmentShipping);
           }*/
         }

         var subject = 'Ship Request: Document #'+checkNull(request.getParameter('custpage_docno'))+' created; Please review and approve'; var body = "";
         body += "<table><tr><td>";
         body += "Dear "+checkNull(request.getParameter('inpt_custpage_approver'))+ ",<br/><br/>A Material request with number "+checkNull(request.getParameter('custpage_docno'))+ " is pending for your approval.<br/>Please find below details:<br/><tr><td>Ship/MT Number: </td><td>"+checkNull(request.getParameter('custpage_docno'))+"</td><tr><td>Requester: </td><td>"+nlapiGetContext().getName()+"</td></tr><tr><td>Department: </td><td>"+checkNull(request.getParameter('inpt_custpage_department'))+"</td></tr><tr><td>Approver: </td><td>"+checkNull(request.getParameter('inpt_custpage_approver'))+"</td></tr><tr><td>Transaction Type: </td><td>"+checkNull(request.getParameter('inpt_custpage_trantype'))+"</td></tr><tr><td>Customer: </td><td>"+checkNull(request.getParameter('inpt_custpage_customer'))+"</td></tr><tr><td>Employee: </td><td>"+checkNull(request.getParameter('inpt_custpage_employee'))+"</td></tr><tr><td>Ship to Address: </td><td>"+checkNull(request.getParameter('custpage_shiptoadd'))+"</td></tr><tr><td>Total Amount: </td><td>"+checkNull(request.getParameter('custpage_total'))+"</td></tr><tr><td>Requester Comment: </td><td>"+checkNull(request.getParameter('custpage_reqcomments'))+"</td></tr>";
         body += "</td></tr></table><br/><br/>Thanks <br/> "+nlapiGetContext().getName()+"";
         if(flag==true && shippingRecId){
           var attachment = emailAttachment(shippingRecId);
           if(attachment)
             nlapiSendEmail(request.getParameter('custpage_requestor'),approverEmail,subject,body,null,null,null);
           else
             nlapiSendEmail(request.getParameter('custpage_requestor'),approverEmail,subject,body,null,null,null,attachment);
         }
         // var postForm = nlapiCreateForm('Shipping/Material Request Form');
         // postForm.addField('custpage_reqsubmitted','text','').setDisplayType('inline').setDefaultValue('Your Shipping Request has been submited for review.');
         // response.writePage(postForm);
       }
       response.write('<html><body><script type="text/javascript">window.onunload = function(e){window.opener.location.reload();}; window.close();</script></body></html>');
     }
   } catch (e) {
     nlapiLogExecution('ERROR', 'Error Caught in Edit Request Suitelet-->', e);
   }
 }

 function checkNull(fldVal) {
   if (fldVal)
     return fldVal;
   else
     return '';
 }

 function searchMTShippingDetails(shippingRequestId) {
   var shippingFilters = [],
     shippingColumns = [];
   shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId));
   shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_deldate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'),new nlobjSearchColumn('custrecord_shipping_details_priorityneed'),new nlobjSearchColumn('custrecord_shipping_details_itar'),new nlobjSearchColumn('custrecord_shipping_details_hazmat'),new nlobjSearchColumn('custrecord_shipping_details_carnet'),new nlobjSearchColumn('custrecord_shipping_details_carnetdetail'));

   var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
   return getShippingDetails;

 }

 function emailAttachment(shippingRecId) {
   try {
     //var shipRequestId = 25; //request.getParameter(name) //Parent Record Id
     var context = nlapiGetContext(),
       emailArr = [];
     //var wareHouseMgrRole = context.getSetting('SCRIPT', 'custscript_ps_whse_manager'),
     //logMgrRole = context.getSetting('SCRIPT', 'custscript_ps_log_manager'),
     //materialsMgrRole = context.getSetting('SCRIPT', 'custscript_ps_mt_manager');
     var configurationObj = nlapiLoadConfiguration('companyinformation');
     var strImageName = 'LiquidRobotics-Boeing-Logo.png'; //configurationObj.getFieldText('formlogo');//'LiquidRobotics-Boeing-Logo.png';
     nlapiLogExecution('DEBUG', 'In POST', 'strImageName-->' + strImageName);
     var imageFilePath = getFilePath(strImageName);
     //imageFilePath = nlapiEscapeXML(nlapiLoadFile(imageId).getURL());

     var companyAddress = checkNull(configurationObj.getFieldValue('attention')) + '<br/>';
     companyAddress += checkNull(configurationObj.getFieldValue('legalname')) + '<br/>';
     companyAddress += checkNull(configurationObj.getFieldValue('address1')) + '<br/>';
     companyAddress += checkNull(configurationObj.getFieldValue('city')) + ' ';
     companyAddress += checkNull(configurationObj.getFieldValue('state')) + ' ';
     companyAddress += checkNull(configurationObj.getFieldValue('zip')) + '<br/>';
     companyAddress += checkNull(configurationObj.getFieldValue('country')) + '<br/> ';
     companyAddress += 'Phone# 408-636-4200<br/> ';
     companyAddress += 'Fax # 408-747-1923<br/> ';

     var companyAddrText = checkNull(configurationObj.getFieldValue('mainaddress_text')); //addresstext
     var companyName = checkNull(configurationObj.getFieldValue('companyname'));

     var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shipRequestId);
     var documentno = checkNull(shipReqObj.getFieldValue('custrecord_document_number'));

     var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">";
     xml += "<pdf>";

     var strVar = '';
     strVar += "<table width='100%'>";
     strVar += "<tr>";
     strVar += "<td width='25%' align='left'>";
     if (imageFilePath)
       strVar += "<img width='100' height= '50' src=\"" + nlapiEscapeXML(imageFilePath) + "\"/>";
     strVar += "<span style=\"font-size:15pt; \">" + companyName + "</span>";
     strVar += "</td>";
     strVar += "<td width='45%'>";
     strVar += "</td>";
     strVar += "<td align='left' width='30%'>";
     strVar += "<span style=\"font-size:15pt; \"><b>Packing Slip</b></span>";
     strVar += "</td>";
     strVar += "</tr>";
     strVar += "<tr>";
     strVar += "<td width='35%'>";
     strVar += companyAddrText; //companyAddress
     strVar += "</td>";
     strVar += "<td width='30%'><b>Requester: </b>" + checkNull(shipReqObj.getFieldText('custrecord_requester')) + "<br/><b>Approver: </b>" + checkNull(shipReqObj.getFieldText('custrecord_approver')) + "";
     strVar += "</td>";
     strVar += "<td width='35%'>";
     strVar += "<b>Order Date:</b> xxxx<br/>";
     strVar += "<b>Order#:</b>" + checkNull(shipReqObj.getFieldValue('custrecord_document_number')) + "";
     strVar += "</td>";
     strVar += "</tr>";
     strVar += "<tr>";
     strVar += "<td align='left'>";
     strVar += "</td>";
     strVar += "<td width='30%'>";
     strVar += "</td>";
     strVar += "<td width='35%' rowspan='4'>";
     strVar += "<b>Ship Date:</b> xxxx<br/>";
     strVar += "<b>Customer:</b>" + checkNull(shipReqObj.getFieldText('custrecord_shipping_customer')) + "<br/>";
     strVar += "<b>Carrier/Service Level :</b>";
     strVar += "</td>";
     strVar += "</tr>";
     strVar += "<tr>";
     strVar += "<td style='word-wrap: break-word'>";
     strVar += "<b>Ship To:</b><br/>" + checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address'));
     strVar += "</td>";
     strVar += "</tr>";
     strVar += "<tr>";
     strVar += "<td>";
     strVar += "<b>Comments:</b>";
     strVar += "</td>";
     strVar += "</tr>";
     strVar += "</table>";

     strVar += "<br/>";
     //Item Table
     strVar += "<table cellborder='0.5' width= '100%'>";
     strVar += "<tr>";
     strVar += "<td><b>LI #</b></td>";
     strVar += "<td><b>Item</b></td>";
     strVar += "<td><b>Description</b></td>";
     strVar += "<td><b>Quantity</b></td>";
     strVar += "</tr>";

     if (nlapiGetRole() == logMgrRole) { //Logistics Manager printing Packing Slip
       var fields = ['custrecord_shipping_details_notified', 'custrecord_shipping_details_shipstatus'],
         fieldValues = ['T', '5'];
       var getLogShippingDetailsRecord = searchLogisticsShippingDetails(shipRequestId);
       for (var shipLogIndex = 0; getLogShippingDetailsRecord && shipLogIndex < getLogShippingDetailsRecord.length; shipLogIndex++) {
         //var emailTo = checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_email'));
         //if(emailTo)
         //emailArr.push(emailTo);
         strVar += "<tr>";
         strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
         strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getText('custrecord_shipping_details_item')) + "</td>";
         strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_description')) + "</td>";
         strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_quantity')) + "</td>";
         strVar += "</tr>";
       }
     }
     strVar += "</table>";
     //xml += "<body font-size=\"8.5\" header=\"myheader\" header-height=\"70px\" footer=\"myfooter\" footer-height=\"10px\">\n"+strVar+"\n</body>\n";
     xml += "<body font-size=\"11pt\" header-height=\"70px\" footer-height=\"10px\">\n" + strVar + "\n</body>\n";
     xml += "</pdf>";

     var file = nlapiXMLToPDF(xml);
     if (file) {
       nlapiLogExecution('DEBUG', 'file-->', file);
       return file;
     }
   } catch (e) {
     nlapiLogExecution('ERROR', 'Error in Packing Slip Suitelet-->', e);
   }
 }

 function searchLogisticsShippingDetails(shippingRequestId) {
   var shippingFilters = [],
     shippingColumns = [];
   shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'));
   shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'));

   var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
   return getShippingDetails;

 }

 function getFilePath(strFileName){
   //nlapiLogExecution("DEBUG", "getFilePath", "strFileName:" + strFileName);
   var retVal = null;

   try
   {
     var objArrSearchColumns = [];
     objArrSearchColumns[0] = new nlobjSearchColumn("internalid");
     objArrSearchColumns[1] = new nlobjSearchColumn("name");


     var objArrSearchFilters = [];
     objArrSearchFilters[0] = new nlobjSearchFilter("name", null, "is",
         strFileName);


     var objArrSearchResult = nlapiSearchRecord("file", null,
         objArrSearchFilters, objArrSearchColumns);

     if(objArrSearchResult && objArrSearchResult.length == 1)
     {
       var strFileID = objArrSearchResult[0].getValue(objArrSearchColumns[0]);
       //nlapiLogExecution("DEBUG", "getFilePath", "strFileID:" + strFileID);
       objFile = nlapiLoadFile(strFileID);

       retVal = objFile.getURL();
 			//nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
     }
   }
   catch (ex)
   {
     // TODO: handle exception
     nlapiLogExecution("ERROR", "getFilePath", ex);
     retVal = null;
   }

   nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
   return retVal;
 }
