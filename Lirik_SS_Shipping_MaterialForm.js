/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       17 Nov 2017     pktandon
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function shippingMaterialRequestForm(request, response) {
  try{
  var objcontext = nlapiGetContext();
  //GET call to view the shipping/material screen
  if (request.getMethod() == 'GET') {
    var currUser = objcontext.getUser();
    var date = new Date();
    //Create Form
    var shipmentForm = nlapiCreateForm('Shipping/Material Request Form');
    //Set client script
    shipmentForm.setScript('customscript_cs_validateshippingdetails');
    var requestor = shipmentForm.addField('custpage_requestor', 'select', 'Requestor', 'employee').setDisplayType('inline');
    requestor.setDefaultValue(currUser);

    var department = shipmentForm.addField('custpage_department', 'select', 'Department', 'department').setDisplaySize('420');
    department.setDefaultValue(nlapiLookupField('employee', currUser, 'department'));
    //	department.setWidth(20);
    var createDate = shipmentForm.addField('custpage_date', 'date', 'Creation Date').setDisplayType('inline');
    createDate.setDefaultValue(date);
    var documentNo = searchLatestDocumentNumber();
    var docnoFld = shipmentForm.addField('custpage_docno', 'integer', 'Document #').setDisplayType('inline');
    if(documentNo)
      docnoFld.setDefaultValue(Number(documentNo)+1);
    var trantype = shipmentForm.addField('custpage_trantype', 'select', 'Transaction Type', 'customlist_shipping_transaction_type').setMandatory(true).setDisplaySize('420');
    var reqComments = shipmentForm.addField('custpage_reqcomments', 'textarea', 'Requestor Comments');
    var logMrgComments = shipmentForm.addField('custpage_logmrgcomm', 'textarea', 'Logistics Manager Comments');
    var matMrgComments = shipmentForm.addField('custpage_matmrgcomm', 'textarea', 'Material Manager Comments').setLayoutType('normal', 'startcol');
    var customerFld = shipmentForm.addField('custpage_customer', 'select', 'Customer', 'customer');
    var employeeFld = shipmentForm.addField('custpage_employee', 'select', 'Employee', 'employee');
    var shipToAdd = shipmentForm.addField('custpage_shiptoadd', 'textarea', 'Ship To Address');
    var shipFromAdd = shipmentForm.addField('custpage_shipfromadd', 'textarea', 'Ship From Address');
    var approver = shipmentForm.addField('custpage_approver', 'select', 'Approver', '').setMandatory(true);
    setApproverList(approver);
    var totalamount = shipmentForm.addField('custpage_total', 'currency', 'Total Amount').setDisplayType('inline').setLayoutType('normal', 'startcol');
    totalamount.setDefaultValue('0.00');

	  var hiddenProjectField = shipmentForm.addField('custpage_project_length','textarea','Project Length').setDisplayType('hidden');

    // Sublist - Details
    var details = shipmentForm.addSubList('custpage_details', 'inlineeditor', 'Items');
    details.addField('custpage_lineseq', 'text', 'Line Sequence #');
    details.addField('custpage_adjuststock', 'select', 'Adjust Stock?(Yes/No)', 'customlist_shipment_request_list');
    details.addField('custpage_item', 'select', 'Item', 'item');
    details.addField('custpage_des', 'textarea', 'Description');
    details.addField('custpage_qty', 'integer', 'Quantity');
    details.addField('custpage_stock', 'select', 'Into or Out Of Stock', 'customlist_shipment_stock_list');
    details.addField('custpage_deliveryddate', 'date', 'Requested Delivery Date');
    details.addField('custpage_shipdate', 'date', 'Requested Ship Date');
    details.addField('custpage_project', 'select', 'Project #', '').setMandatory(true);
    details.addField('custpage_priority', 'text', 'Expedite/Priority needs').setMandatory(true);
    details.addField('custpage_po', 'text', 'PO/RMA/SOD/SODW');
    details.addField('custpage_itar', 'select', 'ITAR/EAR', 'customlist_shipment_request_list').setMandatory(true);
    details.addField('custpage_hazmat', 'select', 'Hazmat Status(Batteries)', 'customlist_shipment_request_list').setMandatory(true);
    details.addField('custpage_carnet', 'select', 'Carnet Desired', 'customlist_shipment_request_list').setMandatory(true);
    details.addField('custpage_carnetdetails', 'textarea', 'Carnet Details');
    details.addField('custpage_email', 'text', 'Email Details').setMandatory(true);
    details.addField('custpage_tracking', 'text', 'Tracking #');
    details.addField('custpage_shipmentdate', 'date', 'Shipment Date');
    details.addField('custpage_shipmentstatus', 'select', 'Shipment Status', 'customlist_shipment_status_list');
    details.addField('custpage_itemcost', 'currency', 'Item Cost');
    details.addField('custpage_comments', 'textarea', 'Comments').setMandatory(true);

    shipmentForm.addSubmitButton('Save');
    response.writePage(shipmentForm);

  }
  //POST call to save the record and send notifications
  else if (request.getMethod() == 'POST') {
	  nlapiLogExecution('DEBUG','Project',request.getLineItemValue('custpage_details','custpage_project',1));
    //Create record
    var custRecord = nlapiCreateRecord('customrecord_shipping_request_record');
    //custRecord.setFieldValue('custrecord_requester', checkNull(request.getParameter('custpage_requestor')));
    var bodyFieldMap = {
      fields: {
        custrecord_requester: 'custpage_requestor',
        custrecord_transaction_type: 'custpage_trantype',
        custrecord_department: 'custpage_department',
        custrecord_creation_date: 'custpage_date',
        custrecord_document_number: 'custpage_docno',
        custrecord_requesters_comments: 'custpage_reqcomments',
        custrecord_shipping_customer: 'custpage_customer',
        custrecord_shipping_employee: 'custpage_employee',
        custrecord_ship_to_address: 'custpage_shiptoadd',
        custrecord_ship_from_address: 'custpage_shipfromadd',
        custrecord_approver: 'custpage_approver',
        custrecord_total: 'custpage_total'
      }
    }
    var bodyKeysFields = Object.keys(bodyFieldMap.fields);
    for (var fieldIndex = 0; fieldIndex < bodyKeysFields.length; fieldIndex++) {
      custRecord.setFieldValue(bodyKeysFields[fieldIndex], checkNull(request.getParameter(bodyFieldMap.fields[bodyKeysFields[fieldIndex]])));
    }
    var shippingRecId = nlapiSubmitRecord(custRecord);
    nlapiLogExecution('DEBUG','In POST','shippingRecId-->'+shippingRecId);
    var lineFieldMap = {
      //sublistId: 'recmachcustrecord_shipping_request_id',
      fields: {
        custrecord_shipping_details_lineseq: 'custpage_lineseq',
        custrecord_shipping_details_adjuststock: 'custpage_adjuststock',
        custrecord_shipping_details_item: 'custpage_item',
        custrecord_shipping_details_description: 'custpage_des',
        custrecord_shipping_details_quantity: 'custpage_qty',
        custrecord_shipping_details_stock: 'custpage_stock',
        custrecord_shipping_details_deldate: 'custpage_deliveryddate',
        custrecord_shipping_details_shipdate: 'custpage_shipdate',
        custrecord_shipping_details_project: 'custpage_project',
        custrecord_shipping_details_priorityneed: 'custpage_priority',
        custrecord_shipping_details_po: 'custpage_po',
        custrecord_shipping_details_itar: 'custpage_itar',
        custrecord_shipping_details_hazmat: 'custpage_hazmat',
        custrecord_shipping_details_carnet: 'custpage_carnet',
        custrecord_shipping_details_carnetdetail: 'custpage_carnetdetails',
        custrecord_shipping_details_email: 'custpage_email',
        //custrecord_shipping_details_tracking: 'custpage_tracking',
        custrecord_shipping_details_shipmentdate: 'custpage_shipmentdate',
        custrecord_shipping_details_shipstatus: 'custpage_shipmentstatus',
        custrecord_shipping_details_itemcost: 'custpage_itemcost',
        custrecord_shipping_details_comments: 'custpage_comments'
      }
    };
    var keysFields = Object.keys(lineFieldMap.fields);

    if(shippingRecId){
      for (var i = 1; request.getLineItemCount('custpage_details')>0 && i <= request.getLineItemCount('custpage_details'); i++) {
        //custRecord.selectNewLineItem('recmachcustrecord_shipping_request_id');
        var childRecord = nlapiCreateRecord('customrecord_shipping_request_details');
        childRecord.setFieldValue('custrecord_shipping_request_id', checkNull(shippingRecId));
        for (var lineFieldIndex = 0; lineFieldIndex < keysFields.length; lineFieldIndex++) {
          childRecord.setFieldValue(keysFields[lineFieldIndex], checkNull(request.getLineItemValue('custpage_details', lineFieldMap.fields[keysFields[lineFieldIndex]], i)));
        }
        //detailRecord.commitLineItem('recmachcustrecord_shipping_request_id');
        var childRecordId = nlapiSubmitRecord(childRecord);
        nlapiLogExecution('DEBUG','In POST','childRecordId-->'+childRecordId);
      }
    }

    //var emailTemplateFromRequester = objcontext.getSetting('SCRIPT','custscript_email_from_requester');

      //response.setContentType('PDF', 'Shipping-Material Request.pdf', 'inline');
      //response.write(xmlContent.getValue());
      if(request.getParameter('custpage_approver') && request.getParameter('custpage_requestor')){
        var approverEmail = nlapiLookupField('employee',request.getParameter('custpage_approver'),'email');
        var email_ShippingToArr = [],email_ToArr = [];
        //email_ShippingToArr.push(approverEmail);
        //email_ToArr.push(approverEmail);
        var flag=false;
        for(var reqIndex=1; reqIndex<request.getLineItemCount('custpage_details'); reqIndex++){
          var adjustStock = request.getLineItemValue('custpage_details','custpage_adjuststock',reqIndex);
          if(adjustStock == '1'){
            flag=true;
            // var emailMTManagers = getMtManagers();
            // var emailTo = request.getLineItemValue('custpage_details','custpage_email',reqIndex);
            // if(emailTo)
            //   email_ToArr.push(emailTo);
            // if(emailMTManagers){
            //   for(var MTIndex=0; MTIndex<emailMTManagers.length; MTIndex++){
            //     email_MTToArr.push(emailMTManagers[MTIndex].getValue('email'));
            //   }
            // }// TODO: create attachment to be sent to approver
            // var attachmentMT = emailAttachment(adjustStock);
          }
          /*else if(adjustStock == '2') {
            var emailTo = request.getLineItemValue('custpage_details','custpage_email',reqIndex);
            if(emailTo)
              email_ShippingToArr.push(emailTo);
            var attachmentShipping = emailAttachment(adjustStock);
            var subject = 'A Shipping Request is pending for your approval', body='Shipping Request is pending for Approval';
            nlapiSendEmail(request.getParameter('custpage_requestor'),request.getParameter('custpage_approver'),subject,body,null,null,null,attachmentShipping);
          }*/
        }

        var subject = 'Ship Request: Document #'+checkNull(request.getParameter('custpage_docno'))+' created; Please review and approve'; var body = "";
        body += "<table><tr><td>";
        body += "Dear "+checkNull(request.getParameter('inpt_custpage_approver'))+ ",<br/><br/>A Material request with number "+checkNull(request.getParameter('custpage_docno'))+ " is pending for your approval.<br/>Please find below details:<br/><tr><td>Ship/MT Number: </td><td>"+checkNull(request.getParameter('custpage_docno'))+"</td><tr><td>Requester: </td><td>"+nlapiGetContext().getName()+"</td></tr><tr><td>Department: </td><td>"+checkNull(request.getParameter('inpt_custpage_department'))+"</td></tr><tr><td>Approver: </td><td>"+checkNull(request.getParameter('inpt_custpage_approver'))+"</td></tr><tr><td>Transaction Type: </td><td>"+checkNull(request.getParameter('inpt_custpage_trantype'))+"</td></tr><tr><td>Customer: </td><td>"+checkNull(request.getParameter('inpt_custpage_customer'))+"</td></tr><tr><td>Employee: </td><td>"+checkNull(request.getParameter('inpt_custpage_employee'))+"</td></tr><tr><td>Ship to Address: </td><td>"+checkNull(request.getParameter('custpage_shiptoadd'))+"</td></tr><tr><td>Total Amount: </td><td>"+checkNull(request.getParameter('custpage_total'))+"</td></tr><tr><td>Requester Comment: </td><td>"+checkNull(request.getParameter('custpage_reqcomments'))+"</td></tr>";
        body += "</td></tr></table><br/><br/>Thanks <br/> "+nlapiGetContext().getName()+"";
        if(flag==true && shippingRecId){
          var attachment = emailAttachment(shippingRecId);
          if(attachment)
            nlapiSendEmail(request.getParameter('custpage_requestor'),approverEmail,subject,body,null,null,null);
          else
            nlapiSendEmail(request.getParameter('custpage_requestor'),approverEmail,subject,body,null,null,null,attachments);
        }
        var postForm = nlapiCreateForm('Shipping/Material Request Form');
        postForm.addField('custpage_reqsubmitted','text','').setDisplayType('inline').setDefaultValue('Your Shipping Request has been submited for review.');
        response.writePage(postForm);
      }
  }
}
catch(e){
  nlapiLogExecution('ERROR','Error caught in Suitelet-->',e);
}
}

//Function to set the Approver List
function setApproverList(approver) {
  var empSearchFilters = [];
  var empSearchColumns = [];

  empSearchFilters.push(new nlobjSearchFilter('custentity_shipping_request_approver',null,'is','T'));
  empSearchColumns.push(new nlobjSearchColumn('internalid'), new nlobjSearchColumn('entityid'));
  var empSearchRec = nlapiSearchRecord('employee', null, empSearchFilters, empSearchColumns);
  for (var i = 0; empSearchRec && (i < empSearchRec.length); i++) {
    approver.addSelectOption(empSearchRec[i].getValue('internalid'), empSearchRec[i].getValue('entityid'));
  }
}

//Function to check null value
function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

//Function to Search Latest Document Number
function searchLatestDocumentNumber() {
  var shippingRequestFilters = [],shippingRequestColumns = [];
  shippingRequestFilters.push(new nlobjSearchFilter('custrecord_document_number',null,'isnotempty'));
  shippingRequestColumns.push(new nlobjSearchColumn('custrecord_document_number').setSort(true));
  var searchRequestShippingRec = nlapiSearchRecord('customrecord_shipping_request_record',null,shippingRequestFilters,shippingRequestColumns);
  nlapiLogExecution('DEBUG','searchLatestDocumentNumber::::',searchRequestShippingRec);
  if(searchRequestShippingRec){
    var docNo = searchRequestShippingRec[0].getValue('custrecord_document_number');
    return docNo;
  }
}

//Function to design attachment
/*function emailAttachment(adjustStock){
if(adjustStock == '1'){
    var headerxml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    var strVar = "";
    strVar += "<table><tr><td>";
    strVar += "Dear "+checkNull(request.getParameter('inpt_custpage_approver'))+ ",<br/><br/>A Material request with number "+checkNull(request.getParameter('custpage_docno'))+ "is pending for your approval.<br/>Please find below details:<br/><tr><td>Ship/MT Number: </td><td>"+checkNull(request.getParameter('custpage_docno'))+"</td><tr><td>Requester: </td><td>"+checkNull(request.getParameter('inpt_custpage_requestor'))+"</td></tr><tr><td>Department: </td><td>"+checkNull(request.getParameter('inpt_custpage_department'))+"</td></tr><tr><td>Approver: </td><td>"+checkNull(request.getParameter('inpt_custpage_approver'))+"</td></tr><tr><td>Transaction Type: </td><td>"+checkNull(request.getParameter('inpt_custpage_trantype'))+"</td></tr><tr><td>Customer/Employee: </td><td>"+checkNull(request.getParameter('inpt_custpage_customer'))+"</td></tr><br/><br/>Thanks & Regards";
    strVar += "</td></tr></table>";
    headerxml += "<body header=\"myheader\" header-height=\"17%\" footer=\"myfooter\" footer-height=\"20pt\" padding=\"0.5in 0.5in 0.5in 0.5in\" size=\"Letter\">\n"+strVar+"\n</body>\n";
    var xmlContent = nlapiXMLToPDF(headerxml);
  }
  return xmlContent;
}*/

function getMtManagers(){
  var MTColumns = [];
  var MTFilters =
  [
   [["entityid","contains","Bahareh Shahabi"],
   "OR",
   ["entityid","contains","Jerome Pereira"]],
   "AND",
   [["email","isnotempty",""]]
 ];
 MTColumns.push(new nlobjSearchColumn('email'));
 var searchMTManagers = nlapiSearchRecord('employee',null,MTFilters,MTColumns);
 if(searchMTManagers){
   return searchMTManagers;
 }
}

function emailAttachment(shippingRecId) {
  try {
    //var shipRequestId = 25; //request.getParameter(name) //Parent Record Id
    var context = nlapiGetContext(),
      emailArr = [];
    //var wareHouseMgrRole = context.getSetting('SCRIPT', 'custscript_ps_whse_manager'),
    //logMgrRole = context.getSetting('SCRIPT', 'custscript_ps_log_manager'),
    //materialsMgrRole = context.getSetting('SCRIPT', 'custscript_ps_mt_manager');
    var configurationObj = nlapiLoadConfiguration('companyinformation');
    var strImageName = 'LiquidRobotics-Boeing-Logo.png'; //configurationObj.getFieldText('formlogo');//'LiquidRobotics-Boeing-Logo.png';
    nlapiLogExecution('DEBUG', 'In POST', 'strImageName-->' + strImageName);
    var imageFilePath = getFilePath(strImageName);
    //imageFilePath = nlapiEscapeXML(nlapiLoadFile(imageId).getURL());

    var companyAddress = checkNull(configurationObj.getFieldValue('attention')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('legalname')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('address1')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('city')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('state')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('zip')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('country')) + '<br/> ';
    companyAddress += 'Phone# 408-636-4200<br/> ';
    companyAddress += 'Fax # 408-747-1923<br/> ';

    var companyAddrText = checkNull(configurationObj.getFieldValue('mainaddress_text')); //addresstext
    var companyName = checkNull(configurationObj.getFieldValue('companyname'));

    var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shipRequestId);
    var documentno = checkNull(shipReqObj.getFieldValue('custrecord_document_number'));

    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">";
    xml += "<pdf>";

    var strVar = '';
    strVar += "<table width='100%'>";
    strVar += "<tr>";
    strVar += "<td width='25%' align='left'>";
    if (imageFilePath)
      strVar += "<img width='100' height= '50' src=\"" + nlapiEscapeXML(imageFilePath) + "\"/>";
    strVar += "<span style=\"font-size:15pt; \">" + companyName + "</span>";
    strVar += "</td>";
    strVar += "<td width='45%'>";
    strVar += "</td>";
    strVar += "<td align='left' width='30%'>";
    strVar += "<span style=\"font-size:15pt; \"><b>Packing Slip</b></span>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td width='35%'>";
    strVar += companyAddrText; //companyAddress
    strVar += "</td>";
    strVar += "<td width='30%'><b>Requester: </b>" + checkNull(shipReqObj.getFieldText('custrecord_requester')) + "<br/><b>Approver: </b>" + checkNull(shipReqObj.getFieldText('custrecord_approver')) + "";
    strVar += "</td>";
    strVar += "<td width='35%'>";
    strVar += "<b>Order Date:</b> xxxx<br/>";
    strVar += "<b>Order#:</b>" + checkNull(shipReqObj.getFieldValue('custrecord_document_number')) + "";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td align='left'>";
    strVar += "</td>";
    strVar += "<td width='30%'>";
    strVar += "</td>";
    strVar += "<td width='35%' rowspan='4'>";
    strVar += "<b>Ship Date:</b> xxxx<br/>";
    strVar += "<b>Customer:</b>" + checkNull(shipReqObj.getFieldText('custrecord_shipping_customer')) + "<br/>";
    strVar += "<b>Carrier/Service Level :</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td style='word-wrap: break-word'>";
    strVar += "<b>Ship To:</b><br/>" + checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address'));
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td>";
    strVar += "<b>Comments:</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "</table>";

    strVar += "<br/>";
    //Item Table
    strVar += "<table cellborder='0.5' width= '100%'>";
    strVar += "<tr>";
    strVar += "<td><b>LI #</b></td>";
    strVar += "<td><b>Item</b></td>";
    strVar += "<td><b>Description</b></td>";
    strVar += "<td><b>Quantity</b></td>";
    strVar += "</tr>";

    if (nlapiGetRole() == logMgrRole) { //Logistics Manager printing Packing Slip
      var fields = ['custrecord_shipping_details_notified', 'custrecord_shipping_details_shipstatus'],
        fieldValues = ['T', '5'];
      var getLogShippingDetailsRecord = searchLogisticsShippingDetails(shipRequestId);
      for (var shipLogIndex = 0; getLogShippingDetailsRecord && shipLogIndex < getLogShippingDetailsRecord.length; shipLogIndex++) {
        //var emailTo = checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_email'));
        //if(emailTo)
        //emailArr.push(emailTo);
        strVar += "<tr>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getText('custrecord_shipping_details_item')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_description')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_quantity')) + "</td>";
        strVar += "</tr>";
      }
    }
    strVar += "</table>";
    //xml += "<body font-size=\"8.5\" header=\"myheader\" header-height=\"70px\" footer=\"myfooter\" footer-height=\"10px\">\n"+strVar+"\n</body>\n";
    xml += "<body font-size=\"11pt\" header-height=\"70px\" footer-height=\"10px\">\n" + strVar + "\n</body>\n";
    xml += "</pdf>";

    var file = nlapiXMLToPDF(xml);
    if (file) {
      nlapiLogExecution('DEBUG', 'file-->', file);
      return file;
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in Packing Slip Suitelet-->', e);
  }
}

function searchLogisticsShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;

}

function getFilePath(strFileName){
  //nlapiLogExecution("DEBUG", "getFilePath", "strFileName:" + strFileName);
  var retVal = null;

  try
  {
    var objArrSearchColumns = [];
    objArrSearchColumns[0] = new nlobjSearchColumn("internalid");
    objArrSearchColumns[1] = new nlobjSearchColumn("name");


    var objArrSearchFilters = [];
    objArrSearchFilters[0] = new nlobjSearchFilter("name", null, "is",
        strFileName);


    var objArrSearchResult = nlapiSearchRecord("file", null,
        objArrSearchFilters, objArrSearchColumns);

    if(objArrSearchResult && objArrSearchResult.length == 1)
    {
      var strFileID = objArrSearchResult[0].getValue(objArrSearchColumns[0]);
      //nlapiLogExecution("DEBUG", "getFilePath", "strFileID:" + strFileID);
      objFile = nlapiLoadFile(strFileID);

      retVal = objFile.getURL();
			//nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
    }
  }
  catch (ex)
  {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "getFilePath", ex);
    retVal = null;
  }

  nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
  return retVal;
}
