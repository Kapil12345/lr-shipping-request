/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Nov 2017     pktandon
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function backendSuitelet(request, response) {
  try {
    var mtManager = nlapiGetContext().getSetting('SCRIPT', 'custscript_mt_manager_role'),
      logisticsManager = nlapiGetContext().getSetting('SCRIPT', 'custscript_logistics_mgr_role'),
      wareHouseManager = nlapiGetContext().getSetting('SCRIPT', 'custscript_whousemgr_role');
    var itemid = '';
    var locCost = '';
    var itemObj = {};
    var itemdes = request.getParameter('custparam_itemDes'),
      itemVal = request.getParameter('custparam_itemval'),
      customer = request.getParameter('custparam_customer'),
      approve = request.getParameter('custparam_approve'),
      reject = request.getParameter('custparam_reject'),
      isItemSerialLot = request.getParameter('custparam_itemVal'),
      sendToRequestor = request.getParameter('custparam_sendtorequestor'),
      sendToWarehouseMgr = request.getParameter('custparam_sendtowhsemgr'),
      lineSequenceNo = request.getParameter('custparam_linesequenceno');
      sendToLogMgr = request.getParameter('custparam_sendtologmgr');
    var parentRecordId = request.getParameter('custparam_parentid');
    var sendToCustomer = request.getParameter('custparam_sendtocustomer');
    var projectSearch = request.getParameter('custparam_projectsearch');
    var empProjectSearch = request.getParameter('custparam_emp_projectsearch');
    nlapiLogExecution('DEBUG', '****Called backEnd Suitelet***', 'approve,reject,parentRecordId,projectSearch,lineSequenceNo' + approve + '###' + reject + '###' + parentRecordId + '###' + isItemSerialLot + '###' + projectSearch+'###'+lineSequenceNo);

    //If block to search item on requester's screen on the basis of item description entered
    if (itemdes) {
      var itemsearchFil = [];
      var itemsearchCol = [];
      itemsearchFil.push(new nlobjSearchFilter('salesdescription', null, 'contains', itemdes));
      itemsearchFil.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', 1));
      itemsearchCol.push(new nlobjSearchColumn('itemid'));
      itemsearchCol.push(new nlobjSearchColumn('internalid'));
      itemsearchCol.push(new nlobjSearchColumn('locationcost'));
      var itemSearchResult = nlapiSearchRecord('item', null, itemsearchFil, itemsearchCol);
      if (itemSearchResult) {
        itemObj.itemid = itemSearchResult[0].getValue('internalid');
        itemObj.locCost = itemSearchResult[0].getValue('locationcost');
      }
      var itemDetails = JSON.stringify(itemObj);
      response.write(itemDetails);
    }
    //Else If block to search item on requester's screen on the basis of item entered
    else if (itemVal) {
      var itemsearchFil = [];
      var itemsearchCol = [];
      itemsearchFil.push(new nlobjSearchFilter('internalid', null, 'anyof', itemVal));
      itemsearchFil.push(new nlobjSearchFilter('inventorylocation', null, 'anyof', 1));
      itemsearchCol.push(new nlobjSearchColumn('itemid'));
      itemsearchCol.push(new nlobjSearchColumn('internalid'));
      itemsearchCol.push(new nlobjSearchColumn('locationcost'));
      var itemSearchResult = nlapiSearchRecord('item', null, itemsearchFil, itemsearchCol);
      if (itemSearchResult) {
        itemObj.itemid = itemSearchResult[0].getValue('internalid');
        itemObj.locCost = itemSearchResult[0].getValue('locationcost');
      }
      var itemDetails = JSON.stringify(itemObj);
      response.write(itemDetails);
    }

    //If block to search Customer to send email
    if (sendToCustomer && parentRecordId) {
      nlapiLogExecution('DEBUG', 'In Customer Attachment>>>', 'sendToCustomer & parentRecordId' + sendToCustomer + '$$' + parentRecordId);
      var shippingRecObj = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
      var customerVal = shippingRecObj.getFieldValue('custrecord_shipping_customer');
      if (customerVal) {
        var customerEmail = nlapiLookupField('customer', customerVal, 'email');
        var customerAttachment = emailAttachment(shippingRecObj, parentRecordId, mtManager, logisticsManager, wareHouseManager, sendToWarehouseMgr, sendToCustomer);
        var subject = 'Ship Request#: ' + shippingRecObj.getFieldValue('custrecord_document_number') + ':' + 'Shipment details';
        var body = 'Please find attached Packing Slip for the Ship request document : ' + shippingRecObj.getFieldValue('custrecord_document_number');
        if (customerAttachment && customerEmail) {
          nlapiLogExecution('DEBUG', 'In Customer Attachment>>>');
          nlapiSendEmail(nlapiGetUser(), customerEmail, subject, body, null, null, null, customerAttachment);
        } else
          nlapiSendEmail(nlapiGetUser(), customerEmail, subject, body);
      }
    }
    //If block to search Customer for shipping address
    else if (customer && !projectSearch) {
      var custFilter = [];
      var custColumn = [];
      var customerObj = {};
      custFilter.push(new nlobjSearchFilter('internalidnumber', null, 'equalto', customer));
      custFilter.push(new nlobjSearchFilter('isdefaultshipping', null, 'is', 'T'));
      custColumn.push(new nlobjSearchColumn('internalid', null, null));
      custColumn.push(new nlobjSearchColumn('address', 'shippingaddress', null));
      var customerSearch = nlapiSearchRecord('customer', null, custFilter, custColumn);
      if (customerSearch) {
        //nlapiLogExecution('DEBUG','customerSearch-->',customerSearch.length);
        //nlapiLogExecution('DEBUG','customerSearch-->',customerSearch[0].getValue('address','shippingaddress',null));
        customerObj.custAdd = customerSearch[0].getValue('address', 'shippingaddress', null);
      }
      var customerDetails = JSON.stringify(customerObj);
      response.write(customerDetails);
    }
    //If block to search Project on the basis of Customer
    else if (customer && projectSearch == 'T') {
      nlapiLogExecution('DEBUG', 'In Project Else Block-->', 'In Customer Block to get Project List');
      var listColumns = [],
        projectListArray = [],
        projectListObj = null; //,listFilters = [];
      var listFilters = [
        [
          [
            ["allowtime", "is", "T"]
          ], "OR", [
            ["allowexpenses", "is", "T"]
          ]
        ], "AND", [
          [
            ["isinactive", "is", "F"]
          ]
        ]
      ];

      //listFilters.push(new nlobjSearchFilter('customer', null, 'anyof', customer));

      listColumns.push(new nlobjSearchColumn('internalid').setSort());
      listColumns.push(new nlobjSearchColumn('entityid'));
      listColumns.push(new nlobjSearchColumn('companyname'));
      listColumns.push(new nlobjSearchColumn('altname'));
      listColumns.push(new nlobjSearchColumn('altname', 'customer'));

      var listValue = nlapiSearchRecord('job', null, listFilters, listColumns);
      //var getSearch = nlapiLoadSearch('job','customsearch_shipping_project_search');
      //nlapiLogExecution('DEBUG','In Project Else Block-->','projectSearch'+JSON.stringify(getSearch.getFilterExpression());
      nlapiLogExecution('DEBUG', 'In Project Else Block-->', 'listValue.length ::' + (listValue ? listValue.length : null));
      var completeResultSet = listValue; //container of the complete result set
      while (listValue.length === 1000) { //re-run the search if limit has been reached
        var lastId = listValue[999].getValue('internalid'); //note the last record retrieved
        listFilters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId)); //create new filter to restrict the next search based on the last record returned
        listValue = nlapiSearchRecord('job', null, listFilters, listColumns);
        completeResultSet = completeResultSet.concat(listValue); //add the result to the complete result set
      }
      for (var projectIndex = 0; completeResultSet && projectIndex < completeResultSet.length; projectIndex++) {
        projectListObj = {};
        var projectIntId = completeResultSet[projectIndex].getValue('internalid');
        var projectID = completeResultSet[projectIndex].getValue('entityid');
        var projectName = completeResultSet[projectIndex].getValue('companyname');
        var projectAltName = completeResultSet[projectIndex].getValue('altname');
        var projectCustName = completeResultSet[projectIndex].getValue('altname', 'customer');

        var projectString = null;
        if (projectName) {
          //projectString = projectID + ' ' + projectCustName + ' ' + ' : ' + ' ' + projectName;
          projectString = projectID + ' ' + projectName;
        } else {
          //projectString = projectID + ' ' + projectCustName + ' ' + ' : ' + ' ' + projectAltName;
          projectString = projectID + ' ' + projectAltName;
        }
        projectListObj.internalId = projectIntId;
        projectListObj.projectName = projectString;
        projectListArray.push(projectListObj);
      }

      nlapiLogExecution('DEBUG', 'In Project List Search -->', JSON.stringify(projectListArray));
      response.write(JSON.stringify(projectListArray));
    }

    //If block to search Project on the basis of Employee
    else if (empProjectSearch == 'T') {
      nlapiLogExecution('DEBUG', 'In Project Else Block-->', 'In Employee Block to get Project List');
      var listColumns = [],
        projectListArray = [],
        projectListObj = null,
        listFilters = [];

      //var listFilters = [[[["allowtime","is","T"]],"OR",[["allowexpenses","is","T"]]],"AND",[[["isinactive","is","F"]]]];
      listFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));

      listColumns.push(new nlobjSearchColumn('internalid').setSort());
      listColumns.push(new nlobjSearchColumn('entityid'));
      listColumns.push(new nlobjSearchColumn('companyname'));
      listColumns.push(new nlobjSearchColumn('altname'));
      listColumns.push(new nlobjSearchColumn('altname', 'customer'));

      var listValue = nlapiSearchRecord('job', null, listFilters, listColumns);
      //var getSearch = nlapiLoadSearch('job','customsearch_shipping_project_search');
      //nlapiLogExecution('DEBUG','In Project Else Block-->','projectSearch'+JSON.stringify(getSearch.getFilterExpression());
      nlapiLogExecution('DEBUG', 'In Project Else Block-->', 'listValue.length ::' + (listValue ? listValue.length : null));
      var completeResultSet = listValue; //container of the complete result set
      while (listValue.length === 1000) { //re-run the search if limit has been reached
        var lastId = listValue[999].getValue('internalid'); //note the last record retrieved
        listFilters.push(new nlobjSearchFilter('internalidnumber', null, 'greaterthan', lastId)); //create new filter to restrict the next search based on the last record returned
        listValue = nlapiSearchRecord('job', null, listFilters, listColumns);
        completeResultSet = completeResultSet.concat(listValue); //add the result to the complete result set
      }

      nlapiLogExecution('DEBUG', 'completeResultSet-->', completeResultSet.length);
      for (var projectIndex = 0; completeResultSet && projectIndex < completeResultSet.length; projectIndex++) {
        projectListObj = {};
        var projectIntId = completeResultSet[projectIndex].getValue('internalid');
        var projectID = completeResultSet[projectIndex].getValue('entityid');
        var projectName = completeResultSet[projectIndex].getValue('companyname');
        var projectAltName = completeResultSet[projectIndex].getValue('altname');
        var projectCustName = completeResultSet[projectIndex].getValue('altname', 'customer');

        var projectString = null;
        if (projectName) {
          //projectString = projectID + ' ' + projectCustName + ' ' + ' : ' + ' ' + projectName;
          projectString = projectID + ' ' + projectName;
        } else {
          //projectString = projectID + ' ' + projectCustName + ' ' + ' : ' + ' ' + projectAltName;
          projectString = projectID + ' ' + projectAltName;
        }
        projectListObj.internalId = projectIntId;
        projectListObj.projectName = projectString;
        projectListArray.push(projectListObj);
      }

      nlapiLogExecution('DEBUG', 'In Project List Search -->', JSON.stringify(projectListArray));
      response.write(JSON.stringify(projectListArray));
    }
    //If block to send email to Warehouse manager
    else if (sendToWarehouseMgr == 'T' && parentRecordId) {
      if (nlapiGetRole() == mtManager) { //Send Email to Warehouse Manager from MT Manager
        var shippingRecObj = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
        var requestorName = nlapiLookupField('employee', shippingRecObj.getFieldValue('custrecord_requester'), 'entityid');
        var getWarehouseManagers = searchWarehouseManagers(wareHouseManager);
        //nlapiLogExecution('DEBUG','getWarehouseManagers-->',getWarehouseManagers[0].getValue('email'));
        var subject = 'Ship/MT#: ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '; Ready for Inventory Adjustment';
        var body = 'Below are the details: <br/>Shipment / MT#: ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '<br/>Requester: ' + checkNull(requestorName) + 'Material Manager Comments: ' + checkNull(shippingRecObj.getFieldValue('custrecord_materials_managers_comments'));
        if (getWarehouseManagers) {
          var getAttachment = emailAttachment(shippingRecObj, parentRecordId, mtManager, logisticsManager, wareHouseManager, sendToWarehouseMgr, sendToCustomer);
          if (getAttachment)
            nlapiSendEmail(nlapiGetUser(), getWarehouseManagers[0].getValue('email'), subject, body, null, null, null, getAttachment);
          else
            nlapiSendEmail(nlapiGetUser(), getWarehouseManagers[0].getValue('email'), subject, body);
        }

      }
    } else if (sendToLogMgr == 'T' && parentRecordId) {
      if (nlapiGetRole() == wareHouseManager) { //Send Email to Logistics Manager from Warehouse Manager
        var shippingRecObj = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
        var requestorName = nlapiLookupField('employee', shippingRecObj.getFieldValue('custrecord_requester'), 'entityid');
        var getLogManagers = searchLogManagers(logisticsManager);
        var subject = 'Ship/MT#: ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '; Ready for Shipping';
        var body = 'Shipment / MT#: ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + 'Inventory Adjustment has been performed and ready for shipping. continue with the shipping activities.';
        if (getLogManagers)
          nlapiSendEmail(nlapiGetUser(), getLogManagers[0].getValue('email'), subject, body);
      }
    }
    //If block called when request is Approved
    else if (approve == 'T' && parentRecordId) {
      var shipRecord = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
      var transactionType = shipRecord.getFieldValue('custrecord_transaction_type'),
        shipReqDocNo = shipRecord.getFieldValue('custrecord_document_number');
      //nlapiLogExecution('DEBUG','transactionType',transactionType);
      if (transactionType == '1') {
        var requester = shipRecord.getFieldValue('custrecord_requester'),
          email_MTToArr = [],
          emailMTManagers = getMtManagers(),
          reqEmail = nlapiLookupField('employee', requester, 'email');
        email_MTToArr.push(reqEmail);
        if (emailMTManagers) {
          for (var MTIndex = 0; MTIndex < emailMTManagers.length; MTIndex++) {
            email_MTToArr.push(emailMTManagers[MTIndex].getValue('email'));
          }
        }
        var emailSubject = 'Document # ' + checkNull(shipReqDocNo) + ' has been approved',
          emailBody = 'Ship/MT Number ' + checkNull(shipReqDocNo) + ' has been approved';
        nlapiLogExecution('DEBUG', 'email_MTToArr', email_MTToArr);
        nlapiSendEmail(nlapiGetUser(), email_MTToArr.toString(), emailSubject, emailBody);
      }
      nlapiSubmitField('customrecord_shipping_request_record', parentRecordId, 'custrecord_is_approved', '1');
    }
    //If block called when request is Rejected
    else if (reject == 'T' && parentRecordId) {
      var shipRecord = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
      var transactionType = shipRecord.getFieldValue('custrecord_transaction_type'),
        shipReqDocNo = shipRecord.getFieldValue('custrecord_document_number');
      //nlapiLogExecution('DEBUG','transactionType',transactionType);
      if (transactionType == '1') {
        var requester = shipRecord.getFieldValue('custrecord_requester'),
          email_MTToArr = [],
          emailMTManagers = getMtManagers(),
          reqEmail = nlapiLookupField('employee', requester, 'email');
        email_MTToArr.push(reqEmail);
        if (emailMTManagers) {
          for (var MTIndex = 0; MTIndex < emailMTManagers.length; MTIndex++) {
            email_MTToArr.push(emailMTManagers[MTIndex].getValue('email'));
          }
        }
        var emailSubject = 'Document # ' + checkNull(shipReqDocNo) + ' has been rejected',
          emailBody = 'Ship/MT Number ' + checkNull(shipReqDocNo) + ' has been rejected';
        nlapiLogExecution('DEBUG', 'email_MTToArr', email_MTToArr);
        nlapiSendEmail(nlapiGetUser(), email_MTToArr.toString(), emailSubject, emailBody);
      }
      if (transactionType == '2') {
        var requester = shipRecord.getFieldValue('custrecord_requester');
        /*,email_MTToArr = [],
              emailMTManagers = getMtManagers(), reqEmail = nlapiLookupField('employee',requester,'email');
              email_MTToArr.push(reqEmail);
              if(emailMTManagers){
                for(var MTIndex=0; MTIndex<emailMTManagers.length; MTIndex++){
                  email_MTToArr.push(emailMTManagers[MTIndex].getValue('email'));
                }
              }*/
        var emailSubject = 'Document # ' + checkNull(shipReqDocNo) + ' has been rejected',
          emailBody = 'Ship/MT Number ' + checkNull(shipReqDocNo) + ' has been rejected';
        nlapiLogExecution('DEBUG', 'email_MTToArr', email_MTToArr);
        nlapiSendEmail(nlapiGetUser(), requester, emailSubject, emailBody);
      }
      nlapiSubmitField('customrecord_shipping_request_record', parentRecordId, 'custrecord_is_approved', '2');
    }
    //If block called when Item is Serialized or Lot
    else if (isItemSerialLot) {
      var fieldArr = ['islotitem', 'isserialitem'];
      var getItemType = nlapiLookupField('item', isItemSerialLot, fieldArr);
      if (getItemType.islotitem == 'T') {
        nlapiLogExecution('DEBUG', 'IS LOT');
        response.write('T');
      } else if (getItemType.isserialitem == 'T') {
        nlapiLogExecution('DEBUG', 'IS SERIAL');
        response.write('T');
      } else {
        nlapiLogExecution('DEBUG', 'IS Else');
        response.write('F');
      }

    }
    //If block called to send Email to Requester
    else if (sendToRequestor == 'T' && parentRecordId) {
      var shippingRecObj = nlapiLoadRecord('customrecord_shipping_request_record', parentRecordId);
      var requestorEmail = nlapiLookupField('employee', shippingRecObj.getFieldValue('custrecord_requester'), 'email');
      if (nlapiGetRole() == mtManager) { //Send Email to requestor from MT Manager
        var subject = 'Ship/MT#: Document # ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '; more information required';
        var body = 'Shipment / MT#: Document # ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '<br/>Material Manager Comments: ' + checkNull(shippingRecObj.getFieldValue('custrecord_materials_managers_comments'));
        if (requestorEmail)
          nlapiSendEmail(nlapiGetUser(), requestorEmail, subject, body);
      } else if (nlapiGetRole() == logisticsManager) { //Send Email to requestor from Log Manager
        var subject = 'Ship/MT#: Document # ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '; more information required';
        var body = 'Shipment / MT#: Document # ' + checkNull(shippingRecObj.getFieldValue('custrecord_document_number')) + '<br/>Logistics Manager Comments: ' + checkNull(shippingRecObj.getFieldValue('custrecord_logistics_managers_comments'));
        if (requestorEmail)
          nlapiSendEmail(nlapiGetUser(), requestorEmail, subject, body);
      }
    }

    //If block called to remove child entry from requester's screen
    else if(lineSequenceNo && parentRecordId){
      var shipDetailsFilters = [];
      shipDetailsFilters.push(new nlobjSearchFilter('custrecord_shipping_details_lineseq',null,'is',lineSequenceNo), new nlobjSearchFilter('custrecord_shipping_request_id',null,'anyof',parentRecordId));
      var searchChildRecordEntry = nlapiSearchRecord('customrecord_shipping_request_details',null,shipDetailsFilters,null);
      if(searchChildRecordEntry){
        //nlapiLogExecution('DEBUG','In Else Block to delete child record',searchChildRecordEntry[0].getId());
        nlapiDeleteRecord('customrecord_shipping_request_details',searchChildRecordEntry[0].getId());
      }
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in Backend Suitelet-->', e);
  }
}

//Function to Get Materials Managers
function getMtManagers() {
  var MTColumns = [];
  var MTFilters = [
    [
      ["entityid", "contains", "Bahareh Shahabi"],
      "OR", ["entityid", "contains", "Jerome Pereira"],
      "OR", ["entityid", "contains", "Anthony Truong"],
      "OR", ["entityid", "contains", "Gabriel Garcia"],
    ],
    "AND", [
      ["email", "isnotempty", ""]
    ]
  ];
  MTColumns.push(new nlobjSearchColumn('email'));
  var searchMTManagers = nlapiSearchRecord('employee', null, MTFilters, MTColumns);
  if (searchMTManagers) {
    return searchMTManagers;
  }
}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

//Function to Get Warehouse Managers
function searchWarehouseManagers(WarehouseManagerRole) {
  var wareHouseManagerColumns = [];
  var wareHouseManagerFilters = [];
  wareHouseManagerFilters.push(new nlobjSearchFilter('entityid', null, 'contains', 'Anthony Truong'), new nlobjSearchFilter('email', null, 'isnotempty'), nlobjSearchFilter('role', null, 'anyof', WarehouseManagerRole));
  wareHouseManagerColumns.push(new nlobjSearchColumn('email'));
  var searchWarehouseManagersResult = nlapiSearchRecord('employee', null, wareHouseManagerFilters, wareHouseManagerColumns);
  if (searchWarehouseManagersResult) {
    return searchWarehouseManagersResult;
  }
}

function searchLogManagers(logisticsManager) {
  var logManagerColumns = [];
  var logManagerFilters = [];
  logManagerFilters.push(new nlobjSearchFilter('entityid', null, 'contains', 'Gabriel Garcia'), new nlobjSearchFilter('email', null, 'isnotempty'), nlobjSearchFilter('role', null, 'anyof', logisticsManager));
  logManagerColumns.push(new nlobjSearchColumn('email'));
  var searchLogManagersResult = nlapiSearchRecord('employee', null, logManagerFilters, logManagerColumns);
  if (searchLogManagersResult) {
    return searchLogManagersResult;
  }
}

//Function to for email Attachment
function emailAttachment(shipReqObj, shipRequestId, materialsMgrRole, logMgrRole, wareHouseMgrRole, sendToWarehouseMgr, sendToCustomer) {
  try {
    //var shipRequestId = 25; //request.getParameter(name) //Parent Record Id
    var context = nlapiGetContext(),
      emailArr = [];
    //var wareHouseMgrRole = context.getSetting('SCRIPT', 'custscript_ps_whse_manager'),
    //logMgrRole = context.getSetting('SCRIPT', 'custscript_ps_log_manager'),
    //materialsMgrRole = context.getSetting('SCRIPT', 'custscript_ps_mt_manager');
    var configurationObj = nlapiLoadConfiguration('companyinformation');
    var strImageName = 'LiquidRobotics-Boeing-Logo.png'; //configurationObj.getFieldText('formlogo');//'LiquidRobotics-Boeing-Logo.png';
    nlapiLogExecution('DEBUG', 'In POST', 'strImageName-->' + strImageName);
    var imageFilePath = getFilePath(strImageName);
    //imageFilePath = nlapiEscapeXML(nlapiLoadFile(imageId).getURL());

    var companyAddress = checkNull(configurationObj.getFieldValue('attention')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('legalname')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('address1')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('city')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('state')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('zip')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('country')) + '<br/> ';
    companyAddress += 'Phone# 408-636-4200<br/> ';
    companyAddress += 'Fax # 408-747-1923<br/> ';

    var companyAddrText = checkNull(configurationObj.getFieldValue('mainaddress_text')); //addresstext
    var companyName = checkNull(configurationObj.getFieldValue('companyname'));

    //var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shipRequestId);
    var documentno = checkNull(shipReqObj.getFieldValue('custrecord_document_number'));

    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">";
    xml += "<pdf>";

    var strVar = '';
    strVar += "<table width='100%'>";
    strVar += "<tr>";
    strVar += "<td width='25%' align='left'>";
    if (imageFilePath)
      strVar += "<img width='100' height= '50' src=\"" + nlapiEscapeXML(imageFilePath) + "\"/>";
    strVar += "<span style=\"font-size:15pt; \">" + companyName + "</span>";
    strVar += "</td>";
    strVar += "<td width='45%'>";
    strVar += "</td>";
    strVar += "<td align='left' width='30%'>";
    strVar += "<span style=\"font-size:15pt; \"><b>Packing Slip</b></span>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td width='35%'>";
    strVar += companyAddrText; //companyAddress
    strVar += "</td>";
    strVar += "<td width='30%'><b>Requester: </b>" + checkNull(shipReqObj.getFieldText('custrecord_requester')) + "<br/><b>Approver: </b>" + checkNull(shipReqObj.getFieldText('custrecord_approver')) + "";
    strVar += "</td>";
    strVar += "<td width='35%'>";
    strVar += "<b>Order Date:</b> xxxx<br/>";
    strVar += "<b>Order#:</b>" + checkNull(shipReqObj.getFieldValue('custrecord_document_number')) + "";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td align='left'>";
    strVar += "</td>";
    strVar += "<td width='30%'>";
    strVar += "</td>";
    strVar += "<td width='35%' rowspan='4'>";
    strVar += "<b>Ship Date:</b> xxxx<br/>";
    strVar += "<b>Customer:</b>" + checkNull(shipReqObj.getFieldText('custrecord_shipping_customer')) + "<br/>";
    strVar += "<b>Carrier/Service Level :</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td style='word-wrap: break-word'>";
    strVar += "<b>Ship To:</b><br/>" + checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address'));
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td>";
    strVar += "<b>Comments:</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "</table>";

    strVar += "<br/>";
    //Item Table
    strVar += "<table cellborder='0.5' width= '100%'>";
    strVar += "<tr>";
    strVar += "<td><b>LI #</b></td>";
    strVar += "<td><b>Item</b></td>";
    strVar += "<td><b>Description</b></td>";
    strVar += "<td><b>Quantity</b></td>";
    strVar += "</tr>";

    if (nlapiGetRole() == logMgrRole) { //Logistics Manager printing Packing Slip
      //var fields = ['custrecord_shipping_details_notified','custrecord_shipping_details_shipstatus'],fieldValues = ['T','5'];
      if (sendToCustomer)
        var getLogShippingDetailsRecord = searchLogisticsShippingDetails(shipRequestId);
      for (var shipLogIndex = 0; getLogShippingDetailsRecord && shipLogIndex < getLogShippingDetailsRecord.length; shipLogIndex++) {
        //var emailTo = checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_email'));
        //if(emailTo)
        //emailArr.push(emailTo);
        strVar += "<tr>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getText('custrecord_shipping_details_item')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_description')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_quantity')) + "</td>";

        //nlapiSubmitField('customrecord_shipping_request_details', getLogShippingDetailsRecord[shipLogIndex].getId(), fields, fieldValues);
        strVar += "</tr>";
      }
    } else if (nlapiGetRole() == materialsMgrRole) { //Material manager sending note to Warehouse manager
      var getMTShippingDetailsRecord = searchMTShippingDetails(shipRequestId);
      for (var shipMTIndex = 0; getMTShippingDetailsRecord && shipMTIndex < getMTShippingDetailsRecord.length; shipMTIndex++) {
        strVar += "<tr>";
        strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
        strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getText('custrecord_shipping_details_item')) + "</td>";
        strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getValue('custrecord_shipping_details_description')) + "</td>";
        strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getValue('custrecord_shipping_details_quantity')) + "</td>";
        strVar += "</tr>";
      }
    }
    strVar += "</table>";
    //xml += "<body font-size=\"8.5\" header=\"myheader\" header-height=\"70px\" footer=\"myfooter\" footer-height=\"10px\">\n"+strVar+"\n</body>\n";
    xml += "<body font-size=\"11pt\" header-height=\"70px\" footer-height=\"10px\">\n" + strVar + "\n</body>\n";
    xml += "</pdf>";

    var file = nlapiXMLToPDF(xml);
    if ((nlapiGetRole() == materialsMgrRole) && (sendToWarehouseMgr == 'T')) {
      nlapiLogExecution('DEBUG', 'file-->', file);
      return file;
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in Packing Slip Suitelet-->', e);
  }
}
//Function to Get MT Child Records
function searchMTShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_adjuststock', null, 'is', '1'), new nlobjSearchFilter('custrecord_shipping_details_inventoryadj', null, 'is', 'T'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;

}

//Function to Get Logistics Child Records
function searchLogisticsShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];

  //if(sendToCustomer)
  //shippingFilters.push(new nlobjSearchFilter('', null, 'anyof', sendToCustomer);
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_printnow', null, 'is', 'F'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;

}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

function getFilePath(strFileName) {
  //nlapiLogExecution("DEBUG", "getFilePath", "strFileName:" + strFileName);
  var retVal = null;

  try {
    var objArrSearchColumns = new Array();
    objArrSearchColumns[0] = new nlobjSearchColumn("internalid");
    objArrSearchColumns[1] = new nlobjSearchColumn("name");


    var objArrSearchFilters = new Array();
    objArrSearchFilters[0] = new nlobjSearchFilter("name", null, "is",
      strFileName);


    var objArrSearchResult = nlapiSearchRecord("file", null,
      objArrSearchFilters, objArrSearchColumns);

    if (objArrSearchResult && objArrSearchResult.length == 1) {
      var strFileID = objArrSearchResult[0].getValue(objArrSearchColumns[0]);
      //nlapiLogExecution("DEBUG", "getFilePath", "strFileID:" + strFileID);
      objFile = nlapiLoadFile(strFileID);

      retVal = objFile.getURL();
      //nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
    }
  } catch (ex) {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "getFilePath", ex);
    retVal = null;
  }

  nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
  return retVal;
}
