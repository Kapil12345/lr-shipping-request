/*******************************************************************
 *
 *
 * Name: Lirik_Shipping_PackingSlip_SS.js
 * Script Type: Suitelet
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used to print Packing Slip
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */

function packingSlipRequest(request, response) {
  try {
    var shipRequestId = request.getParameter('custparam_parentrec_id');
    var context = nlapiGetContext(), emailArr = [];
    var wareHouseMgrRole = context.getSetting('SCRIPT', 'custscript_ps_whse_manager'),
      logMgrRole = context.getSetting('SCRIPT', 'custscript_ps_log_manager'),
      materialsMgrRole = context.getSetting('SCRIPT', 'custscript_ps_mt_manager');
    var configurationObj = nlapiLoadConfiguration('companyinformation');
    var strImageName = 'LiquidRobotics-Boeing-Logo.png'; //configurationObj.getFieldText('formlogo');//'LiquidRobotics-Boeing-Logo.png';
    nlapiLogExecution('DEBUG', 'In POST', 'strImageName-->' + strImageName);
    var imageFilePath = getFilePath(strImageName);
    //imageFilePath = nlapiEscapeXML(nlapiLoadFile(imageId).getURL());

    var companyAddress = checkNull(configurationObj.getFieldValue('attention')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('legalname')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('address1')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('city')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('state')) + ' ';
    companyAddress += checkNull(configurationObj.getFieldValue('zip')) + '<br/>';
    companyAddress += checkNull(configurationObj.getFieldValue('country')) + '<br/> ';
    companyAddress += 'Phone# 408-636-4200<br/> ';
    companyAddress += 'Fax # 408-747-1923<br/> ';

    var companyAddrText = checkNull(configurationObj.getFieldValue('mainaddress_text')); //addresstext
    var companyName = checkNull(configurationObj.getFieldValue('companyname'));

    var shipReqObj = nlapiLoadRecord('customrecord_shipping_request_record', shipRequestId);
    var documentno = checkNull(shipReqObj.getFieldValue('custrecord_document_number'));

    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">";
    xml += "<pdf>";

    var strVar = '';
    strVar += "<table width='100%'>";
    strVar += "<tr>";
    strVar += "<td width='25%' align='left'>";
    if (imageFilePath)
      strVar += "<img width='100' height= '50' src=\"" + nlapiEscapeXML(imageFilePath) + "\"/>";
    strVar += "<span style=\"font-size:15pt; \">" + companyName + "</span>";
    strVar += "</td>";
    strVar += "<td width='45%'>";
    strVar += "</td>";
    strVar += "<td align='left' width='30%'>";
    strVar += "<span style=\"font-size:15pt; \"><b>Packing Slip</b></span>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td width='35%'>";
    strVar += companyAddrText; //companyAddress
    strVar += "</td>";
    strVar += "<td width='30%'><b>Requester: </b>" + checkNull(shipReqObj.getFieldText('custrecord_requester')) + "<br/><b>Approver: </b>"+ checkNull(shipReqObj.getFieldText('custrecord_approver')) + "";
    strVar += "</td>";
    strVar += "<td width='35%'>";
    strVar += "<b>Order Date:</b> xxxx<br/>";
    strVar += "<b>Order#:</b>" + checkNull(shipReqObj.getFieldValue('custrecord_document_number')) + "";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td align='left'>";
    strVar += "</td>";
    strVar += "<td width='30%'>";
    strVar += "</td>";
    strVar += "<td width='35%' rowspan='4'>";
    strVar += "<b>Ship Date:</b> xxxx<br/>";
    strVar += "<b>Customer:</b>" + checkNull(shipReqObj.getFieldText('custrecord_shipping_customer')) + "<br/>";
    strVar += "<b>Carrier/Service Level :</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td style='word-wrap: break-word'>";
    strVar += "<b>Ship To:</b><br/>" + checkNull(shipReqObj.getFieldValue('custrecord_ship_to_address'));
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "<tr>";
    strVar += "<td>";
    strVar += "<b>Comments:</b>";
    strVar += "</td>";
    strVar += "</tr>";
    strVar += "</table>";

    strVar += "<br/>";
    //Item Table
    strVar += "<table cellborder='0.5' width= '100%'>";
    strVar += "<tr>";
    strVar += "<td><b>LI #</b></td>";
    strVar += "<td><b>Item</b></td>";
    strVar += "<td><b>Description</b></td>";
    strVar += "<td><b>Quantity</b></td>";
      strVar += "<td><b>Tracking #</b></td>";
    strVar += "</tr>";

    if (nlapiGetRole() == logMgrRole) {//Logistics Manager printing Packing Slip
      var fields = ['custrecord_shipping_details_notified','custrecord_shipping_details_shipstatus'],fieldValues = ['T','5'];
      var getLogShippingDetailsRecord = searchLogisticsShippingDetails(shipRequestId);
      for (var shipLogIndex = 0; getLogShippingDetailsRecord && shipLogIndex < getLogShippingDetailsRecord.length; shipLogIndex++) {
        var emailTo = checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_email'));
        if(emailTo)
          emailArr.push(emailTo);
        strVar += "<tr>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getText('custrecord_shipping_details_item')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_description')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_quantity')) + "</td>";
        strVar += "<td>" + checkNull(getLogShippingDetailsRecord[shipLogIndex].getValue('custrecord_shipping_details_tracking')) + "</td>";
        nlapiSubmitField('customrecord_shipping_request_details', getLogShippingDetailsRecord[shipLogIndex].getId(), fields, fieldValues);
        strVar += "</tr>";
      }
    }
    /*else if(nlapiGetRole() == materialsMgrRole){//Material manager sending note to Warehouse manager
      var getMTShippingDetailsRecord = searchMTShippingDetails(shipRequestId);
      for (var shipMTIndex = 0; getMTShippingDetailsRecord && shipMTIndex < getMTShippingDetailsRecord.length; shipMTIndex++) {
      strVar += "<tr>";
      strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getValue('custrecord_shipping_details_lineseq')) + "</td>";
      strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getText('custrecord_shipping_details_item')) + "</td>";
      strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getText('custrecord_shipping_details_description')) + "</td>";
      strVar += "<td>" + checkNull(getMTShippingDetailsRecord[shipMTIndex].getText('custrecord_shipping_details_quantity')) + "</td>";
      strVar += "</tr>";
     }
   }*/
    strVar += "</table>";
    //xml += "<body font-size=\"8.5\" header=\"myheader\" header-height=\"70px\" footer=\"myfooter\" footer-height=\"10px\">\n"+strVar+"\n</body>\n";
    xml += "<body font-size=\"11pt\" header-height=\"70px\" footer-height=\"10px\">\n" + strVar + "\n</body>\n";
    xml += "</pdf>";

    var file = nlapiXMLToPDF(xml);
    if(nlapiGetRole() == logMgrRole && emailArr.length>0){
      var subject= 'Packing Slip for the Order # '+checkNull(shipReqObj.getFieldValue('custrecord_document_number')),body = 'Please find the packing slip attached';
      nlapiSendEmail(nlapiGetUser(),emailArr.toString(),subject,body,null,null,null,file);
    }

      response.setContentType('PDF', 'Packing Slip.pdf', 'inline');
      response.write(file.getValue());

    //else if(sendToCustomer){
      //response.setContentType('PDF', 'Packing Slip.pdf', 'attachment');
      //response.write('<html><body><script type="text/javascript">window.close();</script></body></html>');
    //}

  } catch (e) {
    nlapiLogExecution('ERROR', 'Error in Packing Slip Suitelet-->', e);
  }
}

function searchLogisticsShippingDetails(shippingRequestId) {
  var shippingFilters = [],
    shippingColumns = [];
  shippingFilters.push(new nlobjSearchFilter('custrecord_shipping_request_id', null, 'anyof', shippingRequestId), new nlobjSearchFilter('isinactive', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_printnow', null, 'is', 'F'),new nlobjSearchFilter('custrecord_shipping_details_notified', null, 'is', 'F'), new nlobjSearchFilter('custrecord_shipping_details_shipped', null, 'is', 'T'));
  shippingColumns.push(new nlobjSearchColumn('custrecord_shipping_details_inventoryadj'), new nlobjSearchColumn('custrecord_now_future_never'), new nlobjSearchColumn('custrecord_shipping_details_shipdate'), new nlobjSearchColumn('custrecord_shipping_details_lineseq').setSort(), new nlobjSearchColumn('custrecord_shipping_details_item'), new nlobjSearchColumn('custrecord_shipping_details_adjuststock'), new nlobjSearchColumn('custrecord_shipping_details_description'), new nlobjSearchColumn('custrecord_shipping_details_quantity'), new nlobjSearchColumn('custrecord_shipping_details_inventorydet'), new nlobjSearchColumn('custrecord_shipping_details_invent_accno'), new nlobjSearchColumn('custrecord_shipping_details_stock'), new nlobjSearchColumn('custrecord_shipping_details_project'), new nlobjSearchColumn('custrecord_shipping_details_email'), new nlobjSearchColumn('custrecord_shipping_details_shipstatus'), new nlobjSearchColumn('custrecord_shipping_details_itemcost'), new nlobjSearchColumn('custrecord_shipping_details_comments'), new nlobjSearchColumn('custrecord_lirik_inv_adj_recs'),new nlobjSearchColumn('custrecord_shipping_details_tracking'));

  var getShippingDetails = nlapiSearchRecord('customrecord_shipping_request_details', null, shippingFilters, shippingColumns);
  return getShippingDetails;

}

function checkNull(fldVal) {
  if (fldVal)
    return fldVal;
  else
    return '';
}

function getFilePath(strFileName) {
  //nlapiLogExecution("DEBUG", "getFilePath", "strFileName:" + strFileName);
  var retVal = null;

  try {
    var objArrSearchColumns = new Array();
    objArrSearchColumns[0] = new nlobjSearchColumn("internalid");
    objArrSearchColumns[1] = new nlobjSearchColumn("name");


    var objArrSearchFilters = new Array();
    objArrSearchFilters[0] = new nlobjSearchFilter("name", null, "is",
      strFileName);


    var objArrSearchResult = nlapiSearchRecord("file", null,
      objArrSearchFilters, objArrSearchColumns);

    if (objArrSearchResult && objArrSearchResult.length == 1) {
      var strFileID = objArrSearchResult[0].getValue(objArrSearchColumns[0]);
      //nlapiLogExecution("DEBUG", "getFilePath", "strFileID:" + strFileID);
      objFile = nlapiLoadFile(strFileID);

      retVal = objFile.getURL();
      //nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
    }
  } catch (ex) {
    // TODO: handle exception
    nlapiLogExecution("ERROR", "getFilePath", ex);
    retVal = null;
  }

  nlapiLogExecution("DEBUG", "getFilePath", "retVal:" + retVal);
  return retVal;
}
