/*******************************************************************
 *
 *
 * Name: Lirik_UE_Validate_Shipping_request.js
 * Script Type: User Event
 * Version: 1.0
 *
 *
 * Author: Kapil(Lirik Inc.)
 * Purpose: This script is used for validation on Shipping Request Custom Record.
 * Script: The script record id
 * Deploy: The script deployment record id
 *
 *
 * ******************************************************************* */
var context = nlapiGetContext();
var LRIMTManagerRole = context.getSetting('SCRIPT', 'custscript_material_manager_role');
var WarehouseManagerRole = context.getSetting('SCRIPT', 'custscript_warehouse_manager_role');
var LogManagerRole = context.getSetting('SCRIPT', 'custscript_logistics_manager_role');
var employeeCenterRole = context.getSetting('SCRIPT', 'custscript_emp_center_role');
//nlapiLogExecution('Debug','>>>>>>>>',nlapiGetRole+'$$'+context.getRole());
function shipRecordBeforeLoad(type, form) {
  try {
    form.setScript('customscript_lirik_cs_shipping_request');
    if (type == 'view' && (nlapiGetRole() == LRIMTManagerRole || nlapiGetRole() == WarehouseManagerRole || nlapiGetRole() == '3' || nlapiGetRole() == LogManagerRole || nlapiGetRole() == employeeCenterRole)) {
      //nlapiLogExecution('DEBUG','Approved in Before Load-->',nlapiGetFieldValue('custrecord_is_approved'));
      if (nlapiGetRole() != '3')
        form.removeButton('edit');
      var editShippingScreenURL, callInvAdjUrl, callSalesOrderUrl;
      //var invAdjUrl = nlapiResolveURL('SUITELET','customscript_lirik_create_inv_adj_sui','customdeploy_lirik_create_inv_adj_sui');
      if ((context.getRole() == employeeCenterRole) && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // For Employee Center Role
        //nlapiLogExecution('DEBUG','Before Load-->','In Employee Center Role');
        editShippingScreenURL = nlapiResolveURL('SUITELET', 'customscript_lirik_edit_ss_requesterscrn', 'customdeploy_lirik_edit_ss_requesterscrn');
        editShippingScreenURL += '&custparam_emprole=' + employeeCenterRole + '&custparam_requestid=' + nlapiGetRecordId();
      }
      if (context.getRole() == LRIMTManagerRole && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // For Materials Manager
        //form.addButton('custpage_send_to_requester','Send To Requester','');
        editShippingScreenURL = nlapiResolveURL('SUITELET', 'customscript_lirik_edit_shippingreq_scrn', 'customdeploy_lirik_edit_shippingreq_scrn');
        editShippingScreenURL += '&custparam_mtrole=' + LRIMTManagerRole + '&custparam_requestid=' + nlapiGetRecordId();
        //form.addButton('custpage_inv_adj_mtm','Inventory Adjustment');

        var invAdjUrl = nlapiResolveURL('SUITELET', 'customscript_lirik_create_inv_adj_sui', 'customdeploy_lirik_create_inv_adj_sui');
        invAdjUrl = invAdjUrl + '&shipreqid=' + nlapiGetRecordId();
        callInvAdjUrl = "window.open('" + invAdjUrl + "','_self');";
        form.addButton('custpage_inv_adj_mtm', "Inventory Adjustment", 'callInventoryAdjustment()');
        //form.addButton('custpage_inv_adj_mtm', "Inventory Adjustment", callInvAdjUrl);
      } else if (context.getRole() == WarehouseManagerRole && nlapiGetRecordType() == 'customrecord_shipping_request_record') { //For Warehouse Manager
        editShippingScreenURL = nlapiResolveURL('SUITELET', 'customscript_lirik_edit_wmgrshippingreq', 'customdeploy_lirik_edit_wmgrshippingreq');
        editShippingScreenURL += '&custparam_warehouserole=' + WarehouseManagerRole + '&custparam_requestid=' + nlapiGetRecordId();
        //form.addButton('custpage_inv_adj_wm','Inventory Adjustment');

        var invAdjUrl = nlapiResolveURL('SUITELET', 'customscript_lirik_create_inv_adj_sui', 'customdeploy_lirik_create_inv_adj_sui');
        invAdjUrl = invAdjUrl + '&shipreqid=' + nlapiGetRecordId();
        callInvAdjUrl = "window.open('" + invAdjUrl + "','_self');";
        form.addButton('custpage_inv_adj_mtm', "Inventory Adjustment", 'callInventoryAdjustment()'); //callInvAdjUrl
      } else if (context.getRole() == 3 && nlapiGetRecordType() == 'customrecord_shipping_request_record') { //For Administrator
        editShippingScreenURL = nlapiResolveURL('SUITELET', 'customscript_lirik_edit_shippingreq_scrn', 'customdeploy_lirik_edit_shippingreq_scrn');
        editShippingScreenURL += '&custparam_mtrole=' + LRIMTManagerRole + '&custparam_requestid=' + nlapiGetRecordId();
        //form.addButton('custpage_inv_adj_adm','Inventory Adjustment');

        var invAdjUrl = nlapiResolveURL('SUITELET', 'customscript_lirik_create_inv_adj_sui', 'customdeploy_lirik_create_inv_adj_sui');
        invAdjUrl = invAdjUrl + '&shipreqid=' + nlapiGetRecordId();
        callInvAdjUrl = "window.open('" + invAdjUrl + "','_self');";
        form.addButton('custpage_inv_adj_adm', "Inventory Adjustment", 'callInventoryAdjustment()');
      }
      if ((context.getRole() == '3' || context.getRole() == LogManagerRole) && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // For Logistics Manager
        editShippingScreenURL = nlapiResolveURL('SUITELET', 'customscript_lirik_edit_logmgr_view', 'customdeploy_lirik_ss_edit_logmgr_view');
        editShippingScreenURL += '&custparam_logmgrrole=' + LogManagerRole + '&custparam_requestid=' + nlapiGetRecordId();

        var packingSlipURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ship_packingslip_ss', 'customdeploy_lirik_ship_packingslip_ss');
        packingSlipURL += '&custparam_parentrec_id=' + nlapiGetRecordId();

        //var sendToCustomerURL = nlapiResolveURL('SUITELET','customscript_lirik_ship_packingslip_ss','customdeploy_lirik_ship_packingslip_ss');
        //sendToCustomerURL += '&custparam_parentrec_id='+nlapiGetRecordId()+'&custparam_sendtocustomer=T';

        form.addButton('custpage_print_packslip', 'Packing Slip', "window.open('" + packingSlipURL + "');");
        form.addButton('custpage_send_to_customer', 'Send To Customer', 'sendToCustomer()');
      }
      if ((context.getRole() == WarehouseManagerRole || context.getRole() == LRIMTManagerRole || context.getRole() == '3') && nlapiGetRecordType() == 'customrecord_shipping_request_record') {
        var createSOURL = nlapiResolveURL('SUITELET', 'customscript_lirik_ss_create_salesorder', 'customdeploy_lirik_ss_create_sales_order');
        createSOURL += '&shipreqid=' + nlapiGetRecordId();
        callSalesOrderUrl = "window.open('" + createSOURL + "','_self');";
        form.addButton('custpage_createso', 'Create Sales Order & Mark Inactive', 'callSOInactive()'); //callSalesOrderUrl
      }
      if ((context.getRole() == LRIMTManagerRole || context.getRole() == '3' || context.getRole() == LogManagerRole) && nlapiGetRecordType() == 'customrecord_shipping_request_record') {
        form.addButton('custpage_sendtoreq', 'Send To Requester', 'sendEmailToRequester()');
      }
      if (nlapiGetRole() == WarehouseManagerRole && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // Button added for Warehouse Manager to send note to logistics manager to proceed with shipping activities
        form.addButton('custpage_sendtologmgr', 'Send Note to Log Manager', 'sendNoteToLogManager()');
      }
      if ((nlapiGetRole() == '3' || nlapiGetRole() == LRIMTManagerRole) && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // Button added for Material manager to send note to warehouse manager to proceed with inv adjustment
        form.addButton('custpage_sendtoplanner', 'Send Note To Warehouse Manager', 'sendNoteToWarehouseMgr()');
      }

      nlapiLogExecution('DEBUG', 'shipRecordBeforeLoad', 'nlapiGetRecordType() ::' + nlapiGetRecordType());
      nlapiLogExecution('DEBUG', 'shipRecordBeforeLoad', 'nlapiGetRole()::' + nlapiGetRole() + ' ::::::: employeeCenterRole::' + employeeCenterRole);
      nlapiLogExecution('DEBUG', 'shipRecordBeforeLoad', 'nlapiGetUser() ::' + nlapiGetUser());
      nlapiLogExecution('DEBUG', 'shipRecordBeforeLoad', 'nlapiGetFieldValue(custrecord_requester) ::' + nlapiGetFieldValue('custrecord_requester'));
      nlapiLogExecution('DEBUG', 'shipRecordBeforeLoad', 'nlapiGetFieldValue(custrecord_is_approved) ::' + nlapiGetFieldValue('custrecord_is_approved'));

      if (nlapiGetRecordType() == 'customrecord_shipping_request_record' && nlapiGetRole() != employeeCenterRole) {
        nlapiLogExecution('DEBUG', 'before load in other roles');
        form.addButton('custpage_edit', 'Edit', "window.open('" + editShippingScreenURL + "');");
      } else if ((nlapiGetRecordType() == 'customrecord_shipping_request_record') && (nlapiGetRole() == employeeCenterRole) && (nlapiGetUser() == nlapiGetFieldValue('custrecord_requester')) && (nlapiGetFieldValue('custrecord_is_approved') != '1')) {
        nlapiLogExecution('DEBUG', 'before load in emp center role');
        nlapiLogExecution('DEBUG', 'before load', type + '$$' + nlapiGetUser() + '$$' + nlapiGetFieldValue('custrecord_is_approved') + '$$' + nlapiGetFieldValue('custrecord_requester'));
        form.addButton('custpage_edit', 'Edit', "window.open('" + editShippingScreenURL + "');");
      }

    }
    if (type == 'copy' && nlapiGetUser() == nlapiGetFieldValue('custrecord_requester')) { //For Requestor view, if he wants to copy the shipping request
      docNo = searchLatestDocumentNumber();
      nlapiSetFieldValue('custrecord_document_number', Number(docNo) + 1);
    }
    //nlapiLogExecution('DEBUG','before load',type+'$$'+context.getUser()+'$$'+nlapiGetFieldValue('custrecord_approver'))
    if (type == 'view' && nlapiGetRecordType() == 'customrecord_shipping_request_record') { // For Approver view
      var isApproved = nlapiGetFieldValue('custrecord_is_approved');
      //form.setScript('customscript_lirik_cs_shipping_request');
      if (context.getUser() == nlapiGetFieldValue('custrecord_approver')) {
        form.addButton('custpage_approve', 'Approve', 'approveRequest()');
        form.addButton('custpage_reject', 'Reject', 'rejectRequest()');
      }
    } else if (nlapiGetRecordType() == 'customrecord_shipping_request_details' && (type == 'edit' || type == 'xedit') && nlapiGetRole() != '3') { //For Approver view// && (context.getUser() == nlapiGetFieldValue('custrecord_approver'))
      //nlapiLogExecution('DEBUG','In Child Remove button-->');
      nlapiSetRedirectURL('RECORD', 'customrecord_shipping_request_details', nlapiGetRecordId(), false);
    }
  } catch (e) {
    nlapiLogExecution('ERROR', 'Error caught in User event-->', e);
  }
}

function shipRecordBeforeSubmit(type) {}

function shipRecordAfterSubmit(type) {
  nlapiLogExecution('DEBUG', 'After submit type-->', type);
  //nlapiLogExecution('DEBUG','After Submit Field value',nlapiGetFieldValue('custrecord_requester'));
  //to:do add role of approveRequest
  /*var subject = 'Your Request is Rejected';
  var sender = nlapiGetContext().getUser();
  var senderName = nlapiGetContext().getName();
  var receiver = nlapiGetFieldValue('custrecord_requester');
  var receiverText = nlapiGetFieldText('custrecord_requester');
  var mailBody = '<html><body>Hi '+receiverText+',<br/>Your request with number '+nlapiGetFieldValue('custrecord_document_number')+' has been rejected.<br/>Thanks<br/>'+senderName+'</html></body>';

  nlapiSendEmail(sender,receiver,subject,mailBody);*/
}

function searchLatestDocumentNumber() {
  var shippingRequestFilters = [],
    shippingRequestColumns = [];
  shippingRequestFilters.push(new nlobjSearchFilter('custrecord_document_number', null, 'isnotempty'));
  shippingRequestColumns.push(new nlobjSearchColumn('custrecord_document_number').setSort(true));
  var searchRequestShippingRec = nlapiSearchRecord('customrecord_shipping_request_record', null, shippingRequestFilters, shippingRequestColumns);
  nlapiLogExecution('DEBUG', 'searchLatestDocumentNumber::::', searchRequestShippingRec);
  if (searchRequestShippingRec) {
    var docNo = searchRequestShippingRec[0].getValue('custrecord_document_number');
    return docNo;
  }
}
